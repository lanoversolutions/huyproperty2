<?php
  
    return array(
		
		/*----------------| Admin Section |---------------------*/
				
	
			
    	/*----------------|COMMON ACTION LABEL & FLAG MESSAGE SECTION |-----------*/
	    	"LOGIN_SUCCESS" => "LOGIN SUCCESS"
	    	,"ADMIN_ACTION_LABEL_ADD" => "ADD"
			,"ADMIN_ACTION_LABEL_EDIT" => "EDIT"
			,"ADMIN_ACTION_LABEL_UNDO" => "UNDO"
			,"ADMIN_ACTION_LABEL_SELECT" => "SELECT"
			,"ADMIN_ACTION_LABEL_DELETE" => "DELETE"
			,"ADMIN_ACTION_LABEL_REPORT_FROM" => "FROM"
			,"ADMIN_ACTION_LABEL_REPORT_TO" => "TO"
			
		
		/*----------------| Common Section |---------------------*/
			,"ADMIN_LABEL_USER_STATUS_ACTIVE" => "ACTIVE" 
			,"ADMIN_LABEL_USER_STATUS_NOT_ACTIVE" => "NOT ACTIVE" 
			,"ADMIN_LABEL_ACTION" => "Action"
    	
    	
    	/*----------------| HEADER SECTION |---------------------*/
			,"ADMIN_LABEL_HUY_VO_PROPERTY" => "Huy Vo Property"
	    	,"ADMIN_LABEL_HEADING" => "PROPERTY"    	    	    	
	    	,"ADMIN_LABEL_LOGOUT" => "Logout"
	    	,"ADMIN_LABEL_WELCOME" => "WELCOME"
			,"ADMIN_LABEL_LANDLORD"=>"Landlord Manage"
			,"ADMIN_LABEL_TENANT"=>"Tenant Manage"
			,"ADMIN_LABEL_LANDLOARDINDEX"=>"Landlord Listing"
			,"ADMIN_LABEL_TENANTINDEX"=>"Tenant Listing"
			,"ADMIN_LABEL_USER"=>"User Manage"
			,"ADMIN_LABEL_CONTACT" => "Contact"

			
		/*----------------| PAGINATION SECTION |---------------------*/	
	    	,"LABEL_PAGING_PREVIOUS" => "PREVIOUS"	
	    	,"LABEL_PAGING_NEXT" => "NEXT"	
    	
		
		/*----------------| TOP MENU SECTION |---------------------*/
	    	,"ADMIN_MENU_LABEL_DASHBOARD" => "DASHBOARD"		
			,"ADMIN_MENU_LABEL_USER" => "USER"
			,"ADMIN_MENU_LABEL_RECEPTION" => "RECEPTION"
			,"ADMIN_MENU_LABEL_CUSTOMER" => "CUSTOMER"
			,"ADMIN_MENU_LABEL_USERLOG" => "USERLOG"	
		
		/*------------------------|COMPETITOR TYPE|----------------------*/	
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_SUPERIORS" => "SUPERIORS"
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_PEERS" => "PEERS"
			,"FRONT_USER_COMPETITOR_TYPE_LABEL_SUBORDINATES" => "SUBORDINATES"
		
	    	
		/*----------------| LOGIN SECTION |---------------------*/    	    	
	    	,"ADMIN_LABEL_PAGETITLE_LOIGN" => "PROPERTY ADMIN"
			,"USER_LABEL_PAGETITLE_LOIGN" => "PROPERTY USER"    	
	    	,"ADMIN_LABEL_HEADING_LOGIN" => "PROPERTY ADMIN"
	     	,"USER_LABEL_HEADING_LOGIN" => "PROPERTY USER LOGIN"	
	    	,"ADMIN_FORM_LABEL_LOGIN_EMAIL" => "EMAIL"
	    	,"ADMIN_FORM_LABEL_LOGIN_PASSWORD" => "PASSWORD"	    	
			,"ADMIN_FORM_LABEL_LOGIN_LOGINBUTTON" => "LOGIN"
	    	,"ADMIN_LABEL_LOGIN" => "LOGIN"
			
    		
	    	//MESSAGES
    			,"ADMIN_MSG_INVALID_LOGIN" => "INVALID EMAIL OR PASSWORD"
				,"EMPTY_EMAIL" => "Please enter your email"
				,"EMPTY_PASSWORD" => "Please enter your password"	    	    				
    	
		
		/*----------------| DASHBOARD(WELCOME) SECTION |---------------------*/	
    		
			
		,"FRONT_LABEL_HOME"=>"Home"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP1"=>'Property Location'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP2"=>'Property Information'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP3"=>'Amenities and Accessibility'
		,"ADMIN_LINK_LABEL_LANDLORDSTEP4"=>'Add Photos'
		,"FRONT_LABEL_CONTACT" => "Contact"
		,"FRONT_LABEL_HOW_TO_APPLY" => "Apply for Voucher"
		,"FRONT_LABEL_HOW_TO_ABOUT_US" => "About us"
		,"FRONT_LABEL_FIND_PROPERTIES" => ""
		,"FRONT_LABEL_FIND_RENTALS" => "Find-Rentals"
		,"FRONT_LABEL_FIND_LOCAL_TENANT" => "Find Local Tenants"
		,"FRONT_LABEL_PAGETITLE_DASHBOARD" => "Section 8 Housing :- DASHBOARD"

		/*----------------| FRONT FOOTER SECTION |---------------------*/	

		,"FRONT_LABEL_ABOUT_US" => "About us"
		,"FRONT_LABEL_TERMS"=>"Terms and Conditions"
		,"FRONT_LABEL_PRIVANCY"=>"Privacy Policy"
		,"FRONT_LABEL_HELP" => "Help"

		/*----------------| USER SECTION |---------------------*/
		
			### INDEX ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_USER" => "USER LIST"
		    	,"ADMIN_LABEL_HEADING_USER" => "USER LIST"
    	
			### ADD ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_USER_ADD" => "USER ADD"
		    	,"ADMIN_LABEL_HEADING_USER_ADD" => "USER ADD"		
    			
    		### UPDATE ACTION
		    	,"ADMIN_LABEL_PAGETITLE_USER_EDIT" => "USER EDIT"
		    	,"ADMIN_LABEL_HEADING_USER_EDIT" => "USER EDIT"
		
			//LABELS 
				,"ADMIN_LABEL_USER_USERNAME" => "USERNAME"
		    	,"ADMIN_LABEL_USER_PASSWORD" => "PASSWORD"
				,"ADMIN_LABEL_USER_NAME" => "NAME"
				,"ADMIN_LABEL_USER_EMAIL" => "EMAIL"			
		
			//BUTTONS
				,"ADMIN_BUTTON_LABEL_USER_ADD" => "ADD USER"
				,"ADMIN_BUTTON_LABEL_USER_EDIT" => "SAVE CHANGES"
				,"ADMIN_BUTTON_LABEL_USER_SEARCH" => "SEARCH"
					
	    	//LINKS
		    	,"ADMIN_LINK_LABEL_USER_ADD" => "ADD USER"
				,"ADMIN_LINK_LABEL_USER_EDIT" => "EDIT USER"
				,"ADMIN_LINK_LABEL_USER_DELETE" => "DELETE USER"
				,"ADMIN_LINK_LABEL_USER_BACK" => "BACK"
			 		     			
			//MESSAGES
		    	,"ADMIN_MSG_VALID_USER_INSERT" => "RECORD INSERTED SUCCESSFULLY"
				,"ADMIN_MSG_VALID_USER_UPDATE" => "RECORD UPDATED SUCCESSFULLY"    	
		    	,"ADMIN_MSG_VALID_USER_DELETE" => "RECORD DELETED SUCCESSFULLY"	    	
		    	,"ADMIN_MSG_VALID_USER_NO_SELECT_DELETE" => "PLEASE SELECT RECORD(S) TO DELETE"	
	    	
				,"ADMIN_MSG_INVALID_USER_USERNAME" => "USERNAME CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_USER_PASSWORD" => "PASSWORD CAN NOT BE EMPTY!"		
				,"ADMIN_MSG_INVALID_USER_NAME" => "NAME CAN NOT BE EMPTY!"		
				,"ADMIN_MSG_INVALID_USER_EMAIL" => "EMAIL CAN NOT BE EMPTY!"				
				,"ADMIN_MSG_INVALID_USER_SEARCH" => "SEARCH QUERY CAN NOT BE EMPTY!"		
				
			//ALERT MESSAGE 
				,"ADMIN_MSG_ALERT_USER_DELETE" => "DO YOU WANT TO DELETE USER DETAIL?"
				
			
			/*----------------| ForgotPassword SECTION |---------------------------*/	
				
				,"FORM_LABEL_USER_EMAIL"=>"Email"
				,"FORGOT_PASSWORD_LABEL"=>"ForgotPassword"
				,"ADMIN_BUTTON_LABEL_SEND"=>"Send"
				,"EMAIL_ADDRESS_NOT_EXIST"=>"Email address not exist"
				,"FORGOTPASS_MAIL_SEND" => "Please check your email to get an new Password link."
				,"ADMIN_MSG_VALID_NEWPASS" => "Plaese Enter your new password"
				,"ADMIN_MSG_VALID_PASSWORD_RESET" => "Password sucessfully reset"
				,"ADMIN_LABEL_PAGETITLE_RESETPASSWORD" => "Reset Your Password"
				,"ADMIN_LABEL_PAGETITLE_CHECKPASS" => "Check your Password"
								
		/*----------------| MODULE LIST |---------------------------*/
			### LIST MODULES
				,"ADMIN_LABEL_MODULE_NAME_11_1" => "GLOBAL SETTINGS"
			
				,"ADMIN_LABEL_MODULE_NAME_12" => "USERS"
				,"ADMIN_LABEL_MODULE_NAME_13" => "FRONT USERS"
		
		/*----------------| GENERAL SETTING SECTION |---------------------*/
			### INDEX ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS" => "GENERAL SETTINGS LIST"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS" => "GENERAL SETTINGS LIST"
		    	
				### ADD ACTION	
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS_ADD" => "GENERAL SETTINGS ADD"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS_ADD" => "GENERAL SETTINGS ADD"		
		    			
		    	### UPDATE ACTION
		    	,"ADMIN_LABEL_PAGETITLE_GENERALSETTINGS_EDIT" => "GENERAL SETTINGS EDIT"
		    	,"ADMIN_LABEL_HEADING_GENERALSETTINGS_EDIT" => "GENERAL SETTINGS EDIT"
		
		
			//LABELS 
				,"ADMIN_LABEL_GENERALSETTINGS_SITE_TITLE" => "SITE TITLE"
		    	,"ADMIN_LABEL_GENERALSETTINGS_EMAIL_ID" => "EMAIL ID"
		    	,"ADMIN_LABEL_GENERALSETTINGS_CONTACT_NO" => "CONTACT NO"
		    	,"ADMIN_LABEL_GENERALSETTINGS_CONTACT_ADDRESS" => "CONTACT ADDRESS"
				,"ADMIN_LABEL_GENERALSETTINGS_PAGING_VARIABLE" => "PAGING VARIABLE"
		     	,"ADMIN_LABEL_GENERALSETTINGS_PAGE_IN_GROUP" => "PAGE IN GROUP"
				,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE1" => "MASSAGE1 TEXT"
		     	,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE2" => "MASSAGE2 TEXT"
				,"ADMIN_LABEL_GENERALSETTINGS_MESSAGE3" => "MASSAGE3 TEXT"
			
			
			//BUTTONS
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_ADD" => "SUBMIT"
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_EDIT" => "EDIT GENERALS SETTING"
				,"ADMIN_BUTTON_LABEL_GENERALSETTINGS_SEARCH" => "SEARCH"
						
	    	//LINKS
		    	,"ADMIN_LINK_LABEL_GENERALSETTINGS_ADD" => "ADD GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_EDIT" => "EDIT GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_DELETE" => "DELETE GENERALS SETTING"
				,"ADMIN_LINK_LABEL_GENERALSETTINGS_BACK" => "BACK"
			 		     			
			//MESSAGES
		    	,"ADMIN_MSG_VALID_GENERALSETTINGS_INSERT" => "RECORD INSERTED SUCCESSFULLY"
				,"ADMIN_MSG_VALID_GENERALSETTINGS_UPDATE" => "RECORD UPDATED SUCCESSFULLY"    	
		    	,"ADMIN_MSG_VALID_GENERALSETTINGS_DELETE" => "RECORD DELETED SUCCESSFULLY"	    	
		    	
		    	,"ADMIN_MSG_INVALID_GENERALSETTINGS_LABID" => "LAB ID CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_NAME" => "NAME CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_ADDRESS" => "ADDRESS CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_PHONE_NO" => "PHONE NO CAN NOT BE EMPTY!"
				,"ADMIN_MSG_INVALID_GENERALSETTINGS_SEARCH" => "SEARCH QUERY CAN NOT BE EMPTY!"		
					
			//ALERT MESSAGE 
				,"ADMIN_MSG_ALERT_GENERALSETTINGS_DELETE" => "DO YOU WANT TO DELETE GENERALS SETTING DETAIL?"	
		
		
		/*----------------| Login Section |---------------------*/
		
		,"FRONT_LABEL_LOGIN"=>"User Login"
		
		
  		/*----------------| REGISTRATION USER Section |---------------------*/
			### index Action	
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER" => "User Register List"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER" => "User Register List"
				,"ADMIN_LABEL_PAGETITLE_USER_DETAILS" => "User Details"
				,"FRONT_LABEL_REGISTRATION"=> "User Registration"
	    	
			### Add Action	
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_ADD" => "User Registration Settings Add"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER_ADD" => "User Registration Settings Add"
	    			
	    	### Update Action
	    		,"ADMIN_LABEL_PAGETITLE_REGISTRATIONUSER_EDIT" => "User Profile"
	    		,"ADMIN_LABEL_HEADING_REGISTRATIONUSER_EDIT" => "User Profile"
		
		
			//labels 
				,"ADMIN_LABEL_REGISTRATION_FIRST_NAME" => "First Name"
		    	,"ADMIN_LABEL_REGISTRATION_LAST_NAME" => "Last Name"
				,"ADMIN_LABEL_REGISTRATION_EMAIL" => "Email"
				,"ADMIN_LABEL_REGISTRATION_CONFIRM_EMAIL" => "Confirm Email"
				,"ADMIN_LABEL_REGISTRATION_PASSWORD" => "Password"
				,"ADMIN_LABEL_REGISTRATION_CONFIRM_PASSWORD" => "Confirm Password"
				,"ADMIN_LABEL_REGISTRATION_STATUS"=>"Status"
				,"ADMIN_LABEL_REGISTRATION_OPTION"=>"User Option"
				,"ADMIN_LABEL_REGISTRATION_ACTIVE"=>"Active"
				,"ADMIN_LABEL_REGISTRATION_INACTIVE"=>"Inactive"
				,"ADMIN_LABEL_USER_OPTION_LANDLORD"=>"Landlord"
				,"ADMIN_LABEL_USER_OPTION_TENANT"=>"Tenant"
				,"ADMIN_LABEL_REGISTRATION_OLD_PASSWORD" => "Old Password"
				,"ADMIN_LABEL_REGISTRATION_NEW_PASSWORD" => "New Password"
				,"ADMIN_LABEL_REGISTRATION_RETYPE_PASSWORD" => "Re Type Password"
				,"ADMIN_LABEL_REGISTRATION_TYPE" => "USer Type"
		
			//buttons
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_ADD" => "Continue"
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_EDIT" => "Edit"
				,"ADMIN_BUTTON_LABEL_REGISTRATIONUSER_SEARCH" => "Search"
						
		    //links
		    	,"ADMIN_LINK_LABEL_REGISTRATIONUSER_ADD" => "Add User Registration"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_MANAGE" => "Manage User"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_DELETE" => "Delete User Registration"
				,"ADMIN_LINK_LABEL_REGISTRATIONUSER_BACK" => "Back"
				 		     			
			//Messages
		    	,"ADMIN_MSG_VALID_REGISTRATIONUSER_INSERT" => "Registration Successfully. You need to Activate Your account. Please check your email to get an activation link."
				,"ADMIN_MSG_VALID_REGISTRATIONUSER_UPDATE" => "Details Updated Successfully"    	
		    	,"ADMIN_MSG_VALID_REGISTRATIONUSER_DELETE" => "Record Deleted Successfully"	    	
		    	
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_NAME" => "Name can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ADDRESS" => "Address can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PHONE_NO" => "Phone no can not be empty!"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_SEARCH" => "Search Query can not be empty!"
		
					
/*				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ALLREADY_TAKEN" => "User name or Email address allready taken"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NO_MATCH" => "Password must not matched"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NO_BLANK" => "Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME_NO_BLANK" => "First Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NO_BLANK" => "Email address must not not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NO_MATCH" => "Email address must not matched"				
*/				

				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_FIRST_NAME"=>"First Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_LAST_NAME"=>"Last Name must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL"=>"Email address must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_EMAIL"=>"Confirm Email address must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD"=>"Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_CONFORM_PASSWORD"=>"Confirm Password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_ALLREADY_TAKEN"=>"Email Address Already taken"
				,"ADMIN_MSG_INVALID_PASSWORD" => "Old password does not match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_OLD_PASSWORD" => "Old password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_NEW_PASSWORD" => "New password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_RETYPE_PASSWORD" => "Re-type password must not Blank"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_EMAIL_NOT_MATCH" => "Email or Confirm Email do match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_PASSWORD_NOT_MATCH" => "Password or Confirm Password do not match"
				,"ADMIN_MSG_INVALID_REGISTRATIONUSER_INVALID_EMAIL_FORMATE" => "Invalid email address"
			
			//alert message 
				,"ADMIN_MSG_ALERT_REGISTRATIONUSER_DELETE" => "Do you want to delete User Registration detail?"	
	
	
		  /*----------------| Apply for Voucher Section |---------------------*/
			
			
			,"ADMIN_LABEL_HEADING_AGENCY"=>"How to Apply for Section 8 Vouchers -  Section 8 Housing"
			,"ADMIN_PAGETITLE_AGENCY_INDEX" => "How to Apply for Section 8 Vouchers -  Section 8 Housing"
			,"ADMIN_PAGETITLE_AGENCY_KEYWORD"=> "Section 8, Housing Choice Voucher, low income program, tenants, landlord, housing authority"
			,"ADMIN_PAGETITLE_AGENCY_DESCRIPTION"=>"Section 8 Housing is a website for landlords, tenants, and housing authorities who particpate in the  Housing Choice Voucher program."
			
			/*----------------| State Section |---------------------*/
			
			,"ADMIN_LABEL_HEADING_STATE"=>"Apply for Housing Choice Voucher in %%state%% -  Section 8 Housing"
			,"ADMIN_PAGETITLE_STATE_KEYWORD"=>"Section 8, Housing Choice Voucher, low income program, tenants, landlord, housing authority"
			,"ADMIN_PAGETITLE_STATE_DESCRIPTION"=>"Section 8 Housing is a website for landlords, tenants, and housing authorities who particpate in the  Housing Choice Voucher program."
			

	  /*----------------|  Meta Section |---------------------*/
	  ,"ADMIN_LABEL_PAGETITLE_INDEX_INDEX" => "Section 8 Housing"
	  ,"KEYWORD"=> "keywords"
	  ,"ADMIN_PAGETITLE_INDEX_KEYWORD"=> "Section 8, Housing Choice Voucher, low income program, tenants, landlord, housing authority"
	  ,"DESCRIPTION"=>"description"
	  ,"ADMIN_PAGETITLE_INDEX_DESCRIPTION"=>"Section 8 Housing is a website for landlords, tenants, and housing authorities who particpate in the  Housing Choice Voucher program."
	
	
	
   /*----------------|  TenantContact Section |---------------------*/

	,"ADMIN_LABEL_TENANTCONTACT_FIRST_NAME" => "First Name"
	,"ADMIN_LABEL_TENANTCONTACT_LAST_NAME" => "Last Name"
	,"ADMIN_LABEL_TENANTCONTACT_EMAIL" => "Email Address"
	,"ADMIN_LABEL_TENANTCONTACT_PHONE"=> "Phone Number"
	,"ADMIN_LABEL_TENANTCONTACT_BODY"=> "Your message to this landlord"
	,"ADMIN_BUTTON_LABEL_TENANTCONTACT_ADD" => "Send Message"
	,"ADMIN_MSG_VALID_PLEASE_LOGIN" => "Please Login First"
	,"ADMIN_LABEL_REFINE_SEARCH" => "Refine Search"
	,"ADMIN_LABEL_SEARCH" => "Search Results"
	  
	  //Messages
	  ,"ADMIN_MSG_VALID_MAIL_SEND" => "Your Message message has been sent"	
	  ,"ADMIN_MSG_INVALID_TENANTCONTACT_FIRST_NAME"=> "First Name can not be empty!"
	  ,"ADMIN_MSG_INVALID_TENANTCONTACT_LAST_NAME"=> "Last Name can not be empty!"
	  ,"ADMIN_MSG_INVALID_TENANTCONTACT_EMAIL"=> "Email Address can not be empty!"
	  ,"ADMIN_MSG_INVALID_TENANTCONTACT_PHONE"=> "Phone Number can not be empty!"
	  ,"ADMIN_MSG_INVALID_TENANTCONTACT_BODY"=> "Message can not be empty!"	  
	
	 /*----------------| Contact Us Section |---------------------*/
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_CONTACT" => "Contact List"
    	,"ADMIN_LABEL_HEADING_CONTACT_LIST" => "Contact List"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_CONTACT_ADD" => "Contact Us - Section 8 Housing"
    	,"ADMIN_LABEL_HEADING_CONTACT_ADD" => "Contact Us - Section 8 Housing"		
    					
		//labels 
		,"ADMIN_LABEL_CONTACT_NAME" => "Name"
		,"ADMIN_LABEL_CONTACT_EMAIL"=>"Email"
		,"ADMIN_LABEL_CONTACT_SUBJECT"=>"Subject"
		,"ADMIN_LABEL_CONTACT_BODY" => "Body"
		,"ADMIN_LABEL_CONTACT_READ" => "Read"
		,"ADMIN_LABEL_CONTACT_UNREAD" => "Unread"
		,"ADMIN_LABEL_CONTACT_STATUS"=> "Status"
		,"ADMIN_LABEL_CONTACT_CAPTCHA"=>"Captcha"
    	
		//buttons
		,"ADMIN_BUTTON_LABEL_CONTACT_ADD" => "Send Email"
		,"ADMIN_BUTTON_LABEL_CONTACT_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_LINK_LABEL_CONTACT_ADD" => "Send Email"
		,"ADMIN_LINK_LABEL_CONTACT_DELETE" => "Delete Contact Us"
		,"ADMIN_LINK_LABEL_CONTACT_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_CONTACTTITLE_CONTACT_ADD" => "Contact Us"
    	,"ADMIN_LABEL_HEADING_CONTACT_ADD" => "Contact Us"	
		
				
		//Messages
    	,"ADMIN_MSG_VALID_CONTACT_INSERT" => "Thank you for your message. We will address your concern accordingly."
    	,"ADMIN_MSG_VALID_CONTACT_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_CONTACT_CONTACT" => "Contact Us can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"
		,"ADMIN_MSG_INVALID_CONTACT_NAME" => "Name can not be empty!"
		,"ADMIN_MSG_INVALID_CONTACT_EMAIL"=> "Email can not be empty!"
		,"ADMIN_MSG_INVALID_CONTACT_SUBJECT"=> "Subject can not be empty!"
		,"ADMIN_MSG_INVALID_CONTACT_BODY" => "Subject can not be empty!"			
				
		//alert message 
		,"ADMIN_MSG_ALERT_CONTACT_DELETE" => "Do you want to delete Contact Us detail?"    	


	
  		/*----------------|  property Section |---------------------*/
		
		
			### index Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORD" => "Lanlord List"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP1_LIST" => "Lanlord List"
		,"ADMIN_LABEL_PAGETITLE_PROPERTY"=> "Poperty Details"
		
			### Add Action	
	    	,"ADMIN_LABEL_PAGETITLE_PROPERTY_ADD" => "Select Option"
	    	,"ADMIN_LABEL_HEADING_FRONT_USER_ADD" => "Select Option"
		
			//labels 
			,"ADMIN_LABEL_PROPERTY_USER_OPTION" => "Please select any option"
			,"ADMIN_LABEL_LANDLORDSTEP1_CREATED" => "Created Date"
			,"ADMIN_LABEL_LANDLORDSTEP1_MODIFIED" => "Modified Date"
			,"ADMIN_LABEL_APPROVED" => "Approved"
			,"ADMIN_LABEL_PENDING" => "Pending"
			,"ADMIN_LABEL_MAP_STATUS" => "Map Status"
			
			//buttons
			,"ADMIN_BUTTON_LABEL_PROPERTY_ADD" => "Get Started"
			

			//Messages
	    	,"ADMIN_MSG_VALID_PROPERTY_INSERT" => "Saved Successfully"
			,"ADMIN_MSG_VALID_SAVED" => "Succesfully Saved"
			,"ADMIN_MSG_VALID_IMAGE_DELETE" => "Record Deleted Successfully"
			
			//button
			,"ADMIN_LINK_LABEL_LANDLORD_ADD"=>"Add Listing"
			,"ADMIN_LINK_LABEL_LANDLORD_LIST"=>"My Listing"
			,"FRONT_LABEL_DASHBOARD"=> "Dashboard"
			,"FRONT_LABEL_USERPROFILE" => "Edit Profile"
			
			
			/*----------------|  Account activation Section |---------------------*/

		
			,"ADMIN_LABEL_PAGETITLE_ACTIVATIONACCOUNT_EDIT"=> "Account activation"
			,"ADMIN_MSG_VALID_ACTIVATIONACCOUNT_UPDATE"=>"Your account activated successfully"
			
			
			
		   /*----------------| Find-Rentals Section |---------------------*/	
			
			,"ADMIN_BUTTON_LABEL_FRONT_TENANT_SEARCH"=> "Search Rentals"
			,"ADMIN_BUTTON_LABEL_FRONT_FIND_RENTAL"=> "Find Rentals"
			,"ADMIN_BUTTON_LABEL_FRONT_RENTAL_DETAILS" => "Property Details"
			
			//labels 
			
			,"FRONT_LABEL_PROPERTY_DETAILS"=>"Property Details"
			,"FRONT_LABEL_LANDLORD_COMMENTS"=>"Landlord Comments"
			,"FRONT_LABEL_WHO_PAYS_WHAT"=>"Who Pays What?"
			,"FRONT_LABEL_UTILITIES"=>"Utilities"
			,"FRONT_LABEL_ADDITIONAL_DETAILS"=>"Additional Details"
			,"FRONT_LABEL_INDOOR"=>"Indoor"
			,"FRONT_LABEL_KITCHEN"=>"Kitchen"
			,"FRONT_LABEL_OUTDOOR"=>"Outdoor"
			,"FRONT_LABEL_OTHER"=>"Other"
			,"FRONT_LABEL_MAP"=> "Map and Street View"
			,"FRONT_LABEL_PHOTO"=>"Property Photos"
	  	
		  

			/*----------------| Tenant Registertion Section |---------------------*/
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_TENANTREGISTRATION" => "Tenant List"
    	,"ADMIN_LABEL_HEADING_TENANTREGISTRATION_LIST" => "Tenant List"
		,"FRONT_LABEL_MYACCOUNT"=>"My Account"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_TENANTREGISTRATION_ADD" => "Tenant Registertion Add"
    	,"ADMIN_LABEL_HEADING_TENANTREGISTRATION_ADD" => "Tenant Registertion Add"		
    			
    	### Update Action
    	,"ADMIN_LABEL_PAGETITLE_TENANTREGISTRATION_EDIT" => "Tenant Registertion Edit"
    	,"ADMIN_LABEL_HEADING_TENANTREGISTRATION_EDIT" => "Tenant Registertion Edit"	
		
		,"ADMIN_LINK_LABEL_TENANTREGISTRATION_EDITDETAILS" => "Edit Details"
		,"ADMIN_LABEL_HEADING_TENANT" => "Edit Tenant Details"
		
		//labels 
		,"ADMIN_LABEL_TENANTS"=>"Tenants"
		,"ADMIN_LABEL_TENANTREGISTRATION_FIRST_NAME" => "First Name"
    	,"ADMIN_LABEL_TENANTREGISTRATION_LAST_NAME"=>"Last Name"
		,"ADMIN_LABEL_TENANTREGISTRATION_LOCATION"=>"Location"
		,"ADMIN_LABEL_TENANTREGISTRATION_STATE"=>"State"
		,"ADMIN_LABEL_TENANTREGISTRATION_CITY"=>"City"
		,"ADMIN_LABEL_TENANTREGISTRATION_COUNTRY"=>"County"
		,"ADMIN_LABEL_TENANTREGISTRATION_BEDROOMS"=>"Bedrooms"
		,"ADMIN_LABEL_TENANTREGISTRATION_PROPERTY_TYPE" => "Property Type"
		,"ADMIN_LABEL_TENANTREGISTRATION_RENT" => "Rent"
		,"ADMIN_LABEL_TENANTREGISTRATION_PHONE"=>"Phone"
		,"ADMIN_LABEL_TENANTREGISTRATION_SOMETIME_AFTER"=>"Sometime After"
		,"ADMIN_LABEL_TENANTREGISTRATION_VOUCHER_STATUS"=>"Voucher Status"
		,"ADMIN_LABEL_TENANTREGISTRATION_PETS"=>"Pets"
		,"ADMIN_LABEL_TENANTREGISTRATION_ACCESSIBILITY_NEEDS"=>"Accessibility Needs"
		,"ADMIN_LABEL_TENANTREGISTRATION_FREE_LISTING_ALERTS"=>"Free Listing Alerts"
		,"ADMIN_LABEL_TENANTREGISTRATION_STATUS"=>"User status"
		,"ADMIN_LABEL_TENANTREGISTRATION_LAST_ACTIVE" => "Last Active"
		,"ADMIN_BUTTON_LABEL_FRONT_FIND_TENANT" => "Find Local Tenants"
	
		//buttons
		,"ADMIN_BUTTON_LABEL_TENANTREGISTRATION_ADD" => "Create Account"
		,"ADMIN_BUTTON_LABEL_TENANTREGISTRATION_EDIT" => "Edit Tenant Registertion"
		,"ADMIN_BUTTON_LABEL_TENANTREGISTRATION_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_LINK_LABEL_TENANTREGISTRATION_ADD" => "Add Tenant Registertion"
		,"ADMIN_LINK_LABEL_TENANTREGISTRATION_EDIT" => "Save"
		//,"ADMIN_LINK_LABEL_TENANTREGISTRATION_DELETE" => "Delete Tenant Registertion"
		,"ADMIN_LINK_LABEL_TENANTREGISTRATION_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_TENANTREGISTRATIONTITLE_TENANTREGISTRATION_ADD" => "Tenant Registertion Add"
    	,"ADMIN_LABEL_HEADING_TENANTREGISTRATION_ADD" => "Tenant Registertion Add"	
		
		### Update Action
    	,"ADMIN_LABEL_NEWSLATTERTITLE_TENANTREGISTRATION_EDIT" => "Tenant Registertion Edit"
    	,"ADMIN_LABEL_HEADING_NEWSLATTER_EDIT" => "Tenant Registertion Edit"				
		
		//Messages
    	,"ADMIN_MSG_VALID_TENANTREGISTRATION_INSERT" => "Saved Successfully"
		,"ADMIN_MSG_VALID_TENANTREGISTRATION_UPDATE" => "Saved Successfully"    	
    	,"ADMIN_MSG_VALID_TENANTREGISTRATION_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_TENANTREGISTRATION" => "Tenant Registertion can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_FIRST_NAME" => "First Name can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_LAST_NAME" => "Last Name can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_CITY" => "City can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_SOMETIME_AFTER" => "Sometime After can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_VOUCHER_STATUS" => "Voucher Status can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_STATE" => "State can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_COUNTY" => "County can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_BEDROOMS" => "Bedrooms can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_PROPERTY_TYPE" => "Property Type can not be empty!"
		,"ADMIN_MSG_INVALID_TENANTREGISTRATION_RENT" => "Rent can not be empty!"
				
		//alert message 
		,"ADMIN_MSG_ALERT_NEWSLATTER_DELETE" => "Do you want to delete Tenant Registertion detail?"    	
	    
	  
									
	/*----------------| Landlordstep1 Section |---------------------*/
	
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORD" => "Property List"
    	,"ADMIN_LABEL_HEADING_LANDLORD_LIST" => "Property List"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP1_ADD" => "Property Location Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP1_ADD" => "Property Location Add"		
    			
    	### Update Action
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP1_EDIT" => "Property Location Edit"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP1_EDIT" => "Property Location Edit"
		
		//labels
		,"ADMIN_LABEL_LANDLORDS"=>"Landlords" 
		,"ADMIN_LABEL_LANDLORDSTEP1_LANDLORDSTEP1" => "Property Location"
    	,"ADMIN_LABEL_LANDLORDSTEP1_NAME"=>"Name"
		,"ADMIN_LABEL_LANDLORDSTEP1_EMAIL"=>"Email"
		,"ADMIN_LABEL_LANDLORDSTEP1_PHONE"=>"Phone"
		,"ADMIN_LABEL_LANDLORDSTEP1_ADDRESS"=>"Address"
		,"ADMIN_LABEL_LANDLORDSTEP1_UNIT"=>"Unit"
		,"ADMIN_LABEL_LANDLORDSTEP1_CITY"=>"City"
		,"ADMIN_LABEL_LANDLORDSTEP1_STATE"=>"State"
		,"ADMIN_LABEL_LANDLORDSTEP1_ZIP"=>"Zip"
		,"ADMIN_LABEL_LANDLORDSTEP1_NEIGHBORHOOD"=>"Neighborhood"
		,"ADMIN_LABEL_LANDLORDSTEP1_COMMUNITY_NAME"=>"Community Name"
		,"ADMIN_LABEL_LANDLORDSTEP1_STREET_VIEW"=>"Display Street View on Details Page?(Preview)"
		,"ADMIN_LABEL_STATES_SELECT" => "Please select state"
		//,""=>""
				
		//buttons
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP1_ADD" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP1_EDIT" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP1_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_LINK_LABEL_LANDLORDSTEP1_ADD" => "Add Property Location"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP1_EDIT" => "Edit Property Location"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP1_DELETE" => "Delete Property Location"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP1_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_LANDLORDSTEP1TITLE_LANDLORDSTEP1_ADD" => "Property Location Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP1_ADD" => "Property Location Add"	
		
		### Update Action
    	,"ADMIN_LABEL_NEWSLATTERTITLE_LANDLORDSTEP1_EDIT" => "Property Location Edit"
    	,"ADMIN_LABEL_HEADING_NEWSLATTER_EDIT" => "Property Location Edit"				
		
		//Messages
    	,"ADMIN_MSG_VALID_LANDLORDSTEP1_SAVED" => "Step1 Saved Successfully"
		,"ADMIN_MSG_VALID_LANDLORDSTEP1_UPDATE" => "Record Updated Successfully"    	
    	,"ADMIN_MSG_VALID_LANDLORDSTEP1_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_LANDLORDSTEP1" => "Property Location can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"	
		,"ADMIN_MSG_INVALID_LANLORDSTEP1_NAME" => "Name can not be empty!"		
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_EMAIL" => "Email can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_PHONE" => "Phone can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_ADDRESS" => "Address can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_CITY" => "City can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_STATE" => "State can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP1_ZIP" => "Zip can not be empty!"
	
		
				
		//alert message 
		,"ADMIN_MSG_ALERT_NEWSLATTER_DELETE" => "Do you want to delete Property Location detail?"    	
		
		
		
		
			/*----------------| LANDLORDSTEP2 Section |---------------------*/
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP2" => "Property Information List"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP2_LIST" => "Property Information List"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP2_ADD" => "Property Information Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP2_ADD" => "Property Information Add"		
    			
    	### Update Action
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP2_EDIT" => "Property Information Edit"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP2_EDIT" => "Property Information Edit"
		
		//labels 
		,"ADMIN_LABEL_LANDLORDSTEP2_RENT" => "Rent $"
    	,"ADMIN_LABEL_LANDLORDSTEP2_DEPOSIT"=>"Deposit $"
		,"ADMIN_LABEL_LANDLORDSTEP2_DEPOSIT_NEGOTIABLE"=>"Deposit Negotiable?"
		,"ADMIN_LABEL_LANDLORDSTEP2_HIGHLIGHTS"=>"Highlights"
		,"ADMIN_LABEL_LANDLORDSTEP2_BEDROOMS"=>"Bedrooms"
		,"ADMIN_LABEL_LANDLORDSTEP2_BATHROOMS"=>"Bathrooms"
		,"ADMIN_LABEL_LANDLORDSTEP2_YEAR_BUILT"=>"Year Built"
		,"ADMIN_LABEL_LANDLORDSTEP2_DATE_AVAILABLE"=>"Date Available"
		,"ADMIN_LABEL_LANDLORDSTEP2_DESCRIPTION"=>"Description"
		,"ADMIN_LABEL_LANDLORDSTEP2_PROPERTY_TYPE"=>"Property Type"
		,"ADMIN_LABEL_LANDLORDSTEP2_SQUARE_FOOTAGE"=>"Square Footage"
		,"ADMIN_LABEL_LANDLORDSTEP2_LOT_SIZE"=>"Lot Size"
		,"ADMIN_LABEL_LANDLORDSTEP2_FIFTY_FIVE_ONLY"=>"55+ Only?"
		,"ADMIN_LABEL_LANDLORDSTEP2_PET_POLICY"=>"Pets Allowed?"
		,"ADMIN_LABEL_LANDLORDSTEP2_SMOKING_ALLOWED"=>"Smoking Allowed?"
		
		
		//buttons
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP2_ADD" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP2_EDIT" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP2_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_MSG_VALID_LANDLORDSTEP2_SAVED" => "Step2 Saved Successfully"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP2_EDIT" => "Edit Property Information"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP2_DELETE" => "Delete Property Information"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP2_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_LANDLORDSTEP2TITLE_LANDLORDSTEP2_ADD" => "Property Information Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP2_ADD" => "Property Information Add"	
		
		### Update Action
    	,"ADMIN_LABEL_NEWSLATTERTITLE_LANDLORDSTEP2_EDIT" => "Property Information Edit"
    	,"ADMIN_LABEL_HEADING_NEWSLATTER_EDIT" => "Property Information Edit"				
		
		//Messages
    	,"ADMIN_MSG_VALID_LANDLORDSTEP2_INSERT" => "Record Inserted Successfully"
		,"ADMIN_MSG_VALID_LANDLORDSTEP2_UPDATE" => "Record Updated Successfully"    	
    	,"ADMIN_MSG_VALID_LANDLORDSTEP2_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_LANDLORDSTEP2_LANDLORDSTEP2" => "Property Information can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"			
		,"ADMIN_MSG_INVALID_LANDLORDSTEP2_RENT" => "Rent can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP2_BEDROOMS" => "Bedroom can not be empty!"
		
				
		//alert message 
		,"ADMIN_MSG_ALERT_NEWSLATTER_DELETE" => "Do you want to delete Property Information detail?"    	

	
	
			/*----------------| LANDLORDSTEP3 Section |---------------------*/
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP3" => "Amenities and Accessibility List"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP3_LIST" => "Amenities and Accessibility List"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP3_ADD" => "Amenities and Accessibility Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP3_ADD" => "Amenities and Accessibility Add"		
    			
    	### Update Action
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP3_EDIT" => "Amenities and Accessibility Edit"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP3_EDIT" => "Amenities and Accessibility Edit"
		
		//labels 
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_CEILING_FANS" => "Ceiling Fans"
		,"ADMIN_LABEL_LANDLORDSTEP3_DEPOSIT_INDOOR_FUNISHED"=>"Furnished"
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_FIREPLACE"=>"Fireplace"
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_CABLE_INCLUDED"=>"Cable Included"
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_SECURITY_SYSTEM"=>"Security System"
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_LAUDRY"=>"Laundry Type"
		,"ADMIN_LABEL_LANDLORDSTEP3_INDOOR_HEATING_TYPE"=>"Heating Type"
		,"ADMIN_LABEL_LANDLORDSTEP3_KITCHEN_DISHWASHER"=>"Dishwasher"
		,"ADMIN_LABEL_LANDLORDSTEP3_KITCHEN_STOVE"=>"Stove"
		,"ADMIN_LABEL_LANDLORDSTEP3_KITCHEN_GARBAGE_DISPOSAL"=>"Garbage Disposal"
		,"ADMIN_LABEL_LANDLORDSTEP3_KITCHEN_REFRIGERATOR"=>"Refrigerator"
		,"ADMIN_LABEL_LANDLORDSTEP3_KITCHEN_MICROWAVE"=>"Microwave"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_SWIMMING_POOL"=>"Swimming Pool"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_GATED_COMMUNITY"=>"Gated Community"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_LAWN_CARE_INCLUDED"=>"Lawn Care Included "
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_TRASH_REMOVAL_INCLUDED"=>"Trash Removal Included"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_FENCED_YARD"=>"Fenced Yard"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_PARKONG_TYPE"=>"Parking Type"
		,"ADMIN_LABEL_LANDLORDSTEP3_OUTDOOR_PARKONG_PATIO"=>"Patio/Porch"
		,"ADMIN_LABEL_LANDLORDSTEP3_SMOKING_ALLOWED"=>"Pest Control"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_ELECTRIC"=>"Electric"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_HEATING_FUEL"=>"Heating Fuel"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_WATER"=>"Water"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_HOT_WATER"=>"Hot Water"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_COOKING_FUEL"=>"Cooking Fuel"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_SEWER"=>"Sewer"
		,"ADMIN_LABEL_LANDLORDSTEP3_UTILITIES_COOLING"=>"Cooling"
		
    	
		//buttons
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP3_ADD" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP3_EDIT" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP3_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_LINK_LABEL_LANDLORDSTEP3_ADD" => "Add Amenities and Accessibility"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP3_EDIT" => "Edit Amenities and Accessibility"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP3_DELETE" => "Delete Amenities and Accessibility"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP3_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_LANDLORDSTEP3TITLE_LANDLORDSTEP3_ADD" => "Amenities and Accessibility Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP3_ADD" => "Amenities and Accessibility Add"	
		
		### Update Action
    	,"ADMIN_LABEL_NEWSLATTERTITLE_LANDLORDSTEP3_EDIT" => "Amenities and Accessibility Edit"
    	,"ADMIN_LABEL_HEADING_NEWSLATTER_EDIT" => "Amenities and Accessibility Edit"				
		
		//Messages
    	,"ADMIN_MSG_VALID_LANDLORDSTEP3_SAVED" => "Step3 Saved Successfully"
		,"ADMIN_MSG_VALID_LANDLORDSTEP3_UPDATE" => "Record Updated Successfully"    	
    	,"ADMIN_MSG_VALID_LANDLORDSTEP3_DELETE" => "Record Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_LANDLORDSTEP3" => "Amenities and Accessibility can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HEATING_FUEL_RENT" => "Heating fual can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_WATER_RENT" => "Water rent can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HOT_WATER_RENT" => "Hot water can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOKING_FUEL_RENT" => "Cooking fual can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_SEWER_RENT" => "Sewer can not be empty!"
		,"ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOLING_RENT" => "Colling can not be empty!"
							
				
		//alert message 
		,"ADMIN_MSG_ALERT_NEWSLATTER_DELETE" => "Do you want to delete Amenities and Accessibility detail?"    	

	
	
				/*----------------| LANDLORDSTEP4 Section |---------------------*/
		### index Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP4" => "Add Photos List"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP4_LIST" => "Add Photos List"
    	
		### Add Action	
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP4_ADD" => "Add Photos Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP4_ADD" => "Add Photos Add"		
    			
    	### Update Action
    	,"ADMIN_LABEL_PAGETITLE_LANDLORDSTEP4_EDIT" => "Add Photos Edit"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP4_EDIT" => "Add Photos Edit"
		
		//labels 
		,"ADMIN_LABEL_LANDLORDSTEP4_URL" => "Select Photo"
		,"ADMIN_LABEL_LANDLORDSTEP4_IMAGE" => "Image"
		
		//buttons
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP4_ADD" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP4_EDIT" => "Save"
		,"ADMIN_BUTTON_LABEL_LANDLORDSTEP4_SEARCH" => "Search"
				
    	//links
    	,"ADMIN_LINK_LABEL_LANDLORDSTEP4_ADD" => "Add Photos"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP4_EDIT" => "Edit Add Photos"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP4_DELETE" => "Delete Add Photos"
		,"ADMIN_LINK_LABEL_LANDLORDSTEP4_BACK" => "Back"
			    	
		### Add Action	
    	,"ADMIN_LABEL_LANDLORDSTEP4TITLE_LANDLORDSTEP4_ADD" => "Add Photos Add"
    	,"ADMIN_LABEL_HEADING_LANDLORDSTEP4_ADD" => "Add Photos Add"	
		
		### Update Action
    	,"ADMIN_LABEL_NEWSLATTERTITLE_LANDLORDSTEP4_EDIT" => "Add Photos Edit"
    	,"ADMIN_LABEL_HEADING_NEWSLATTER_EDIT" => "Add Photos Edit"				
		
		//Messages
    	,"ADMIN_MSG_VALID_LANDLORDSTEP4_SAVED" => "Step4 Saved Successfully"
		,"ADMIN_MSG_VALID_LANDLORDSTEP4_UPDATE" => "Record Updated Successfully"    	
    	,"ADMIN_MSG_SUCCESS_IMAGE_DELETED" => "Image Deleted Successfully"
		
		,"ADMIN_MSG_INVALID_LANDLORDSTEP4_LANDLORDSTEP4" => "Add Photos can not be empty!"				
		,"ADMIN_MSG_INVALID_COMMENT_SEARCH" => "Search Query can not be empty!"			
				
		//alert message 
		,"ADMIN_MSG_ALERT_LANDLORDSTEP4_DELETE" => "Do you want to delete Image?"    	
	
						
   );
?>