<?php

/**
 * Class for property_amenities table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_PropertyAmenities extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'property_amenities'; 		
                   
  	
	 /**
     * Fetch Particular property_amenities Detail
     *	 
     */
    /*----------------------| Get spacific property Detail   |----------------------*/
    public function fetchEntry($id,$user_id='') {
		
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('pa'=>'property_amenities'),array('pa.*'))
				->joinLeft(array('p'=>'property'), 'pa.property_id  = p.id', array('p.user_id'))											
				->where('pa.property_id='.$id.' AND p.user_id='.$user_id);    
				
		
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }
	

    /**
     * Update data for property_amenities entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = " property_id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
    
	
	
		/**
    * Save data for new property_amenities entry.
    *
    * @param  array $data Array of property_amenities data to insert in database.
    * @return integer|boolean Last inserted author id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		$data['modified'] = date('Y-m-d H:i:s');  
		$data['created'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }

	
	
	
	/**
     * Delete data forom property_amenities entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteData($id,$user_id='') {									
		$objData = $this->fetchRow('property_id='.$id);		
		if($objData)
			$objData->delete();
		else
      		return null;    
		unset($objData);
  	}

 	
}
?>
