<?php

/**
 * Class for SendEmail table related database operations.
 *
 * @category   PS
 * @package    Models_SendEmail
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_SendEmail
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	//protected $_name = 'list_email'; 		
     	
	/*----------------------| send email to taskcanon.com |---------------*/		
	public function sendEmail($to='', $mail_subject='', $mail_body='',$headers='')
	{
		
		//set POST variables
		$url = 'http://taskcanon.com/email.php';
		$fields = array(
								'to'=>$to,
								'mail_subject'=>$mail_subject,
								'mail_body'=>$mail_body,
								'headers'=>$headers
						);
		
		//url-ify the data for the POST
		$fields_string='';
		foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
		rtrim($fields_string,'&');
		
		//open connection
		$ch = curl_init();
		
		//set the url, number of POST vars, POST data
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST,count($fields));
		curl_setopt($ch,CURLOPT_POSTFIELDS,$fields_string);
		
		//execute post
		$result = curl_exec($ch);
		
		//close connection
		curl_close($ch);
  	}

}
?>
