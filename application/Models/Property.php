<?php

/**
 * Class for property table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Property extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'property'; 		
   
   
   	/**
     * Get all property listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "p.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('p'=>'property'),array('p.*'))
				//->joinLeft(array('pp'=>'property_photo'), 'p.id  = pp.property_id', array('pp.url'))								
				->where('user_id = '.$user_id);	
				
					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('p.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
  	


   	/**
     * Get all property listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getLanlordList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "p.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('p'=>'property'),array('p.*'));
							
					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('p.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}

	
	
	 /**
     * Fetch Particular property Detail
     *	 
     */
    /*----------------------| Get spacific property Detail   |----------------------*/
    public function fetchEntry($id,$user_id = '') {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('p'=>'property'),array('p.*'))								
				->where('p.user_id = '.$user_id.' AND p.id='.$id);
			    	
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }


	 /**
     * Fetch Particular propertydetails Detail
     *	 
     */
    /*----------------------| Get spacific propertydetails Detail   |----------------------*/
    public function fetchdetails($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('p'=>'property'),array('p.*'))
				->joinLeft(array('pa'=>'property_amenities'), 'p.id  = pa.property_id', array('pa.*'))
				->joinLeft(array('pp'=>'property_photo'), 'p.id  = pp.property_id', array('pp.*'))							
				->where('p.id = '.$id);
		
		//echo $select; exit;	    	
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }



    /**
     * Update data for property entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id,$user_id='') { 
		$where = "id = " . $id ." AND user_id = ".$user_id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		if(isset($data['date_available']))
		$data['date_available'] = date('Y-m-d' , strtotime($data['date_available']));															
		$data['modified'] = date('Y-m-d H:i:s');
		
		//echo $where; exit;						
		return $this->update($data,$where);
    }
    
		/**
    * Save data for new property entry.
    *
    * @param  array $data Array of property data to insert in database.
    * @return integer|boolean Last inserted property id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        } 
		
		if(isset($data['id']) && $data['id'] == '')
			unset($data['id']);  
		
		if(isset($data['date_available']))
		$data['date_available'] = date('Y-m-d' , strtotime($data['date_available']));
				
		$data['modified'] = date('Y-m-d H:i:s');  
		$data['created'] = date('Y-m-d H:i:s');                        
		
        return $this->insert($data);
     }



	/**
     * Delete data forom property entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteData($id,$user_id='') {									
		$objData = $this->fetchRow('id='.$id ." AND user_id = ".$user_id);		
		$objData->delete();
		unset($objData);
  	}
	
	
		/**
     * Delete data forom property entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteproertyData($id) {									
		$objData = $this->fetchRow('id='.$id);		
		if($objData)
			$objData->delete();
		else
      		return null;    
		unset($objData);
	}


	 	/**
     * Make subject Combobox
     *	 
     */
    /*----------------------| Get States Combobox   |----------------------*/
    public function getStateCombobox() {
		
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.id','s.name'))
				->order('s.name');												
    	
    	$select = $this->fetchAll($select);
		$select = $select->toArray();				
		
		$arrStates = array();
		$arrStates[''] = $objTranslate->translate('ADMIN_LABEL_STATES_SELECT');
		foreach($select as $row){
			$arrStates[$row['id']] = $row['name'];
		}		
		//_pr($arrStates,1);
		if($arrStates)
	    	return $arrStates;
		else
      		return null;    
    }



	 	/**
     * Make subject Combobox
     *	 
     */
    /*----------------------| Get States Combobox   |----------------------*/
    public function getCountyCombobox($state_id = null) {
		
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'counties'),array('c.id','c.name'))
				->where('c.state_id = '.$state_id)
				->order('c.name');												    	
    	$select = $this->fetchAll($select);
		$select = $select->toArray();				
		
		$arrCounty = array();
		$arrCounty[''] = 'Please select County';
		foreach($select as $row){
			$arrCounty[$row['id']] = $row['name'];
		}		
				
		if($arrCounty)
	    	return $arrCounty;
		else
      		return null;    
    }

 
 
 	 /**
     * Fetch Particular propertydetails Detail
     *	 
     */
    /*----------------------| Get spacific propertydetails Detail   |----------------------*/
    public function fetchstatus($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('p'=>'property'),array('p.*'))
				->where('p.id = '.$id);
		
		//echo $select; exit;	    	
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }

 
 
    /**
     * Update data for property entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updatestatus(array $data,$id) { 
		$where = "id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		if(isset($data['date_available']))
		$data['date_available'] = date('Y-m-d' , strtotime($data['date_available']));															
		$data['modified'] = date('Y-m-d H:i:s');
		
		//echo $where; exit;						
		return $this->update($data,$where);
    }


}
?>
