<?php

/**
 * Class for Tenantregister table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Tenantregister extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'tenant'; 		
                   
 
  	   	/**
     * Get all tenant listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getTenantList($searchText='',$searchType='',$sortby='',$user_id='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "t.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('t'=>'tenant'),array('t.*'));
		
	    //echo $select;exit;			
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('t.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}

	
	
	
	
	 /**
     * Fetch Particular Tenantregister Detail
     *	 
     */
    /*----------------------| Get spacific Tenantregister Detail   |----------------------*/
    public function fetchEntry($id,$user_id = '') {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('t'=>'tenant'),array('t.*'))
				//->joinLeft(array('c'=>'counties'), 't.county = c.id', array('c.id as county_id','c.name as county'))								
				->where('t.user_id = '.$user_id);
				
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	

	 /**
     * Fetch Particular Tenantregister Detail
     *	 
     */
    /*----------------------| Get spacific Tenantregister Detail   |----------------------*/
    public function fetchTenant($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('t'=>'tenant'),array('t.*'))								
				->where('t.user_id = '.$id);
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }


	
	
	/**
    * Save data for new Tenantregister entry.
    *
    * @param  array $data Array of Tenantregister data to insert in database.
    * @return integer|boolean Last inserted author id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }
	
	

    /**
     * Update data for Tenantregister entry.
     *
     * @param  array $data Array of Tenantregister data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id='') { 
		$where = "user_id = '".$id."'";
		
		//$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		//_pr($data,1);
		//echo $data; exit;																			
		return $this->update($data,$where);
    }



	    /**
     * Update data for Tenantregister entry.
     *
     * @param  array $data Array of Tenantregister data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateTenant(array $data,$id) { 
		$where = " id = '".$id."'";
		
		$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																					
		return $this->update($data,$where);
    }



		/**
     * Delete data forom Tenant entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteTenantData($id) {									
		$objData = $this->fetchRow('id='.$id);		
		if($objData)
			$objData->delete();
		else
      		return null;    
		unset($objData);
	}
	
			 	      	
}
?>
