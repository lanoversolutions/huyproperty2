<?php

/**
 * Class for property_photo table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_PropertyPhoto extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'property_photo'; 		
                   
  	
	 /**
     * Fetch Particular property_photo Detail
     *	 
     */
    /*----------------------| Get spacific property_photo Detail   |----------------------*/
    public function fetchEntry($id='',$user_id='') {
		
		
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('pp'=>'property_photo'),array('pp.*'))								
				->joinLeft(array('p'=>'property'), 'pp.property_id  = p.id', array('p.user_id'))											
				->where('pp.property_id='.$id.' AND p.user_id='.$user_id);    
		
		//echo $select;exit;		    	
    	$select = $this->fetchAll($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }
	
	
		 /**
     * Fetch Particular property_photo Detail
     *	 
     */
    /*----------------------| Get spacific property_photo Detail   |----------------------*/
    public function fetchimage($id) {
	
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('pp'=>'property_photo'),array('pp.*'))																			
				->where('pp.id='.$id);    
		
		//echo $select;exit;		    	
    	$select = $this->fetchRow($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }



		 /**
     * Fetch Particular property_photo Detail
     *	 
     */
    /*----------------------| Get spacific property_photo Detail   |----------------------*/
    public function fetchimages($id) {
	
    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('pp'=>'property_photo'),array('pp.*'))																			
				->where('pp.property_id='.$id);    
		
		//echo $select;exit;		    	
    	$select = $this->fetchAll($select);

		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }

    /**
     * Update data for property_photo entry.
     *
     * @param  array $data Array of property_photo data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		//$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
    
	
	
		/**
    * Save data for new property_photo entry.
    *
    * @param  array $data Array of property_photo data to insert in database.
    * @return integer|boolean Last inserted property_photo id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value){
           if (!in_array($field, $fields)) 
                unset($data[$field]);
		}
		$data['modified'] = date('Y-m-d H:i:s');  
		$data['created'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }


	/**
     * Delete data forom images entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteimages($id) {									
		$objData = $this->fetchRow('id ='.$id);
			
		if($objData)
			$objData->delete();
		else
      		return null;    
    
		unset($objData);
  	}

	/**
     * Delete data forom property_photo entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteData($id) {
		
		$select = $this->fetchAll('property_id ='.$id);
		$objData = $select->toArray();

		foreach ( $objData as $data ):
		
		if($data)
		 $this->delete('property_id ='.$id);
		else
    	 return null;    

		endforeach;
			
		unset($objData);	   
	
		
  	}

 	
}
?>
