<?php

/**
 * Class for User table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Contact extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'contact'; 		
                   
	/**
     * Get all front_user listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='',$searchType='',$sortby='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $search_type = addslashes($searchType);	  
	 $cond_string = "c.".$search_type." like '%".$search_txt."%'";		 	 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('c'=>'contact'),array('c.*'));								
				//->where('is_admin != 1');
								 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('c.status DESC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
  
  
   	
	 /**
     * Fetch Particular front_user Detail
     *	 
     */
    /*----------------------| Get spacific front_user Detail   |----------------------*/
    public function fetchEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('c'=>'contact'),array('c.*'))								
				//->where('u.is_admin != 1')
				->where('c.id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }
	
	
	
	
	
	/**
    * Save data for new front_user entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		if(isset($data['id']) && $data['id'] == '')
			unset($data['id']);  
		
		$data['created_at'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }
			 	     	
	
	    /**
     * Update data for user entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																		
		return $this->update($data,$where);
    }
    

	
	
	/**
     * Delete data forom front_user entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteData($id) {									
		$objData = $this->fetchRow('id='.$id);		
		$objData->delete();
		unset($objData);
  	}

 	
}
?>
