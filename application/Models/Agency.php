<?php

/**
 * Class for User table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Agency extends PS_Database_Table
{  
    /**#@+
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'states'; 		
                   
	/**
     * Get all front_user listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList()
	{ 	 										    	    	    	   		 
	 		 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('s'=>'states'),array('s.statecode','s.filename'));								
				//->where('is_admin != 1');
		echo $select;exit;					 	 	 				 
		if($search_txt != '' && $search_type != '')
			$select->where($cond_string);
	 
		if(trim($sortby)=='')
			$select->order('s.id ASC');
		else
			$select->order($strSort);
	//echo $select;exit;	 	 			
			
	 return $select;	 	    				
  	}
  
  
  
  
    	 /**
     * Fetch Particular email Detail
     *	 
     */
    /*----------------------| Get spacific email Detail   |----------------------*/
	
	public function fetchstates($filename)
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('s'=>'states'),array('s.statecode AS scode','s.filename AS fname','s.name AS sname'))
				->joinLeft(array('a'=>'agencies'), 's.statecode = a.statecode', array('a.*',"CONCAT(a.name,' ','Housing Agency')AS agenciesname "))								
				->where('s.filename = ?', $filename);
		
		//echo $select; exit;
		$select = $this->fetchAll($select);
		
		if ($select){
				return $select = $select->toArray(); 
		}else{
			return null;
	   }
	}

  
  
  
  
  	 /**
     * Fetch Particular email Detail
     *	 
     */
    /*----------------------| Get spacific email Detail   |----------------------*/
	
	public function fetchstate($State)
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('a'=>'agencies'),array('a.*'))								
				->where('a.statecode = ?', $State);
		
		$select = $this->fetchAll($select);
		
		if ($select){
				return $select = $select->toArray(); 
		}else{
			return null;
	   }
	}

  
  	
	 /**
     * Fetch Particular front_user Detail
     *	 
     */
    /*----------------------| Get spacific front_user Detail   |----------------------*/
    public function fetchEntry($id) {

    	$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('u'=>'front_user'),array('u.*'))								
				//->where('u.is_admin != 1')
				->where('u.id = ?',$id);										
    	
    	$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }
	
	
	
		 /**
     * Check existing record
     *	 
     */
   	public function checkRedudancy($fields)
	{
		/*
		* 1 => Password not same
		* 2 => Email address is already exist
		* 0 => Okay
		*/
		if(($fields['email'] != $fields['conform_email']))
			return 1;
		
		if(($fields['password'] != $fields['conform_password']))
			return 3;
		
		$search_field = "u.email ='".$fields['email']."'";
		
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('u'=>'front_user'),array('u.*'))								
				->where($search_field);
			
		//echo $select; exit;
		$select = $this->fetchAll($select);
		$select = $select->toArray(); 
		//_pr($select,1);
		return ($select)?2:0; 
	
	}
	
		 /**
     * Fetch Particular confirm_key Detail
     *	 
     */
    /*----------------------| Get spacific confirm_key Detail   |----------------------*/
	
	public function fetchkey($id)
	{
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('u'=>'front_user'),array('u.*'))								
				->where('u.confirm_key = ?', $id);
				
		$select = $this->fetchRow($select);
		$select = $select->toArray();
		
		if ($select){
				return $select;
		}else{
			return null;
	   }
	}

  	
	 
	
	
	/**
    * Save data for new front_user entry.
    *
    * @param  array $data Array of front_user data to insert in database.
    * @return integer|boolean Last inserted front_user id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		if(isset($data['email']) && $data['email'] != "") {
			$data['password'] = md5($data['password']);
		}
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }
			 	     


    /**
     * Update data for front_user entry.
     *
     * @param  array $data Array of page data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id) { 
		$where = "user_id = " . $id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
    
	
	/**
     * Delete data forom front_user entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	/*public function deleteData($id) {									
		$objData = $this->fetchRow('id='.$id);		
		$objData->delete();
		unset($objData);
  	}
*/
 	
}
?>
