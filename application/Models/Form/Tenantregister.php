<?php

//class Models_Form_Tenantregister extends PS_Form
class Models_Form_Tenantregister extends PS_Form
{

	public function init()
	{
	    parent::init();
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);

		//$this->setMethod('post');
		global $VoucherStatusTypeList;
		global $arrLandlordType;
		global $arrBedroom;
		global $arrProperty_Type;

		$objRegFirstName = new Zend_Form_Element_Text('first_name');
		$objRegFirstName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_FIRST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objRegLsatName = new Zend_Form_Element_Text('last_name');
		$objRegLsatName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_LAST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		


		$objRegLocation = new Zend_Form_Element_Text('location');
		$objRegLocation
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_CITY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objState = new Zend_Form_Element_Select('state_id');
		$objState
	        ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_STATE'))))
			->setRegisterInArrayValidator(false);


		$objCounty = new Zend_Form_Element_Select('county');
		$objCounty
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_COUNTY'))))		
			->addMultiOptions(array())
			->setRegisterInArrayValidator(false);


		$objRegCity = new Zend_Form_Element_Text('city');
		$objRegCity
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_CITY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		$objBedrooms = new Zend_Form_Element_Select('bedrooms');
		$objBedrooms
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_BEDROOMS'))))			
			->addMultiOptions($arrBedroom);


		$objProperty_type = new Zend_Form_Element_Select('property_type');
		$objProperty_type
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_PROPERTY_TYPE'))))			
			->addMultiOptions($arrProperty_Type);


		$objRent = new Zend_Form_Element_Text('rent');
		$objRent
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_RENT'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objRegPhone = new Zend_Form_Element_Text('phone');
		$objRegPhone
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objRegSometimeAfter = new Zend_Form_Element_Text('sometime_after');
		$objRegSometimeAfter
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_SOMETIME_AFTER'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
			
		$objRegVoucherStatus = new Zend_Form_Element_Select('voucher_status');
		$objRegVoucherStatus
			->setRequired(true)			
			->setAttrib('class','form-selectbox validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_VOUCHER_STATUS'))))						
			->addMultiOptions($VoucherStatusTypeList);
		
	
		$objRegPets = new Zend_Form_Element_Radio('pets');
		$objRegPets
			->setAttrib('class','')
			->setAttrib('id', 'optionsRadios1')						
			->addMultiOptions($arrLandlordType)
			->setValue('no');																						


		$objAccessibilityNeeds = new Zend_Form_Element_Radio('accessibility_needs');
		$objAccessibilityNeeds
			->setAttrib('class','')					
			->addMultiOptions($arrLandlordType)
			->setValue('no');																						
	
	
		$objFreeListingAlerts = new Zend_Form_Element_Radio('free_listing_alerts');
		$objFreeListingAlerts
			->setAttrib('class','')
			//->setAttrib('id', 'optionsRadios1')						
			->addMultiOptions( array("Both Email and Phone" => "Both Email and Phone","Email" => "Email","Phone"=>"Phone","I do not want to be contacted"=>"I do not want to be contacted") )
			->setValue('Phone');																						

			
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_TENANTREGISTRATION_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			

		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_LINK_LABEL_TENANTREGISTRATION_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
			
					

		$this->addElements(array($objRegFirstName,$objRegLsatName,$objState,$objRegCity,$objCounty,$objBedrooms,$objProperty_type,$objRent,$objRegPhone,$objRegSometimeAfter,$objRegVoucherStatus,$objRegPets,$objAccessibilityNeeds,$objFreeListingAlerts,$objAddButton,$objEditButton));
	}
}
