<?php

//class Models_Form_Tenantregister extends PS_Form
class Models_Form_Tenant extends PS_Form
{

	public function Listsearch()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');


		global $arrSearchProperty_Type;
	    global $arrSearchBedrooms;
		global $arrSearchBathroom;
		global $arrSquare_footage_min;
		global $arrSquare_footage_max;

		/*$SearchTypeList = array(
			"name"=>'Name',
			"city"=>'City',						
		);*/
						
		$objTxtSearch = new Zend_Form_Element_Text('txtsearch');
		$objTxtSearch
			->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('size','50px')
			->setAttrib('placeholder','Please enter City or State or ZIP')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

	    $objRent = new Zend_Form_Element_Text('rent');
		$objRent
			->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('placeholder','Please enter Max Rent')			
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objSquare_footage = new Zend_Form_Element_Text('square_footage');
		$objSquare_footage
			->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('placeholder','Please enter Square Footage')			
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');




		$objSquare_footage = new Zend_Form_Element_Text('square_footage');
		$objSquare_footage
			->setRequired(true)
			->setAttrib('class','form-textbox')
			->setAttrib('placeholder','Please enter Square Footage')			
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		$objSquare_footage_min = new Zend_Form_Element_Select('square_footage_min');
		$objSquare_footage_min
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))			
     		->addMultiOptions($arrSquare_footage_min);


		$objSquare_footage_max = new Zend_Form_Element_Select('square_footage_max');
		$objSquare_footage_max
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))			
     		->addMultiOptions($arrSquare_footage_max);
			

		$objProperty_type = new Zend_Form_Element_Multiselect('property_type');
		$objProperty_type
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			//->setAttrib('multiple','multiple')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANT_SEARCH'))))			
     		->addMultiOptions($arrSearchProperty_Type);



		$objBedrooms = new Zend_Form_Element_Select('bedrooms');
		$objBedrooms
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP2_BEDROOMS'))))			
			->addMultiOptions($arrSearchBedrooms);

		$objBathrooms = new Zend_Form_Element_Select('bathrooms');
		$objBathrooms
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required] validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP2_BEDROOMS'))))						
     		->addMultiOptions($arrSearchBathroom);

		
		/*$objSearchType = new Zend_Form_Element_Select('searchtype');
		$objSearchType
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($SearchTypeList);*/
			
		
		$objSearchButton = new Zend_Form_Element_Submit('search',$objTranslate->_('ADMIN_BUTTON_LABEL_FRONT_TENANT_SEARCH'));
		$objSearchButton
            ->setAttrib('id', 'search')
            ->setAttrib('class','btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
				
		
		$this->addElements(array($objTxtSearch,$objRent,$objSquare_footage ,$objSquare_footage_min,$objSquare_footage_max,$objProperty_type,$objBedrooms,$objBathrooms,$objSearchButton));
		
	}


	//Landlordstep2 form
	
 public function tenantcontact()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');

		$objFirst_Name = new Zend_Form_Element_Text('first_name');
		$objFirst_Name
	        ->setRequired(true)
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTCONTACT_FIRST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		

		$objLast_Name = new Zend_Form_Element_Text('last_name');
		$objLast_Name
	        ->setRequired(true)
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTCONTACT_LAST_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		
		$objEmail = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)
	        ->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			->addValidator('EmailAddress',  TRUE  )
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTCONTACT_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
		$objPhone = new Zend_Form_Element_Text('phone');
		$objPhone
	        ->setRequired(true)			
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTCONTACT_PHONE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
																						
		$objBody = new Zend_Form_Element_Textarea('body');
		$objBody
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTCONTACT_BODY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
				
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_TENANTCONTACT_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

				$this->clearElements();	

		$this->addElements(array( $objLast_Name, $objFirst_Name , $objEmail ,$objPhone ,$objBody ,$objAddButton));
				
	}




	//localtenants form
	
 public function localtenants()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');

	    global $arrBedroom;


		$objState = new Zend_Form_Element_Select('state_id');
		$objState
	        ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]');
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_STATE'))));


		$objCounty = new Zend_Form_Element_Select('county');
		$objCounty
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_COUNTY'))))			
			->addMultiOptions(array());
			

		$objBedrooms = new Zend_Form_Element_Select('bedrooms');
		$objBedrooms
	        ->setRequired(true)		
			->setAttrib('class','form-selectbox validate[required]')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_TENANTREGISTRATION_BEDROOMS'))))			
			->addMultiOptions($arrBedroom);
			
				
		$objSearchButton = new Zend_Form_Element_Submit('search',$objTranslate->_('ADMIN_BUTTON_LABEL_FRONT_TENANT_SEARCH'));
		$objSearchButton
            ->setAttrib('id', 'search')
            ->setAttrib('class','btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

				$this->clearElements();	

		$this->addElements(array( $objState, $objCounty , $objBedrooms ,$objSearchButton));
				
	}

}
?>