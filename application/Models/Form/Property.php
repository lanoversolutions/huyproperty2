<?php

//class Models_Form_Property extends PS_Form

class Models_Form_Property extends PS_Form
{

	public function init()
	{
	    parent::init();
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);

		//$this->setMethod('post');
	   global $arrUserOption;

		$objUserOption = new Zend_Form_Element_Radio('user_option');
		$objUserOption
	        ->setRequired(true)
			->setAttrib('class','validate[required]')
			->setAttrib('id', 'optionsRadios1')						
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_PROPERTY_OPTION'))))
			->addMultiOptions($arrUserOption)
			->setValue('1');																						

			
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_PROPERTY_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
					

		$this->addElements(array($objUserOption,$objAddButton));
	}
	
	
	//Landlordstep1 form

	
		public function Landlordstep1()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$this->setMethod('post');
		global $arrLandlordType;
		
		$objName = new Zend_Form_Element_Text('name');
		$objName
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANLORDSTEP1_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objEmail = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objPhone = new Zend_Form_Element_Text('phone');
		$objPhone
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_PHONE'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objAddress = new Zend_Form_Element_Text('address');
		$objAddress
	        ->setRequired(true)
			->setAttrib('class','validate[required]')
			->setAttrib('id','gadres')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_ADDRESS'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objUnit = new Zend_Form_Element_Text('unit');
		$objUnit
	        //->setRequired(true)
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_UNIT'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objCity = new Zend_Form_Element_Text('city');
		$objCity
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[onlyLetterSp]]')
			->setAttrib('id','gcity')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_CITY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objState = new Zend_Form_Element_Select('state_id');
		$objState
	        ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_STATE'))));


		$objZip = new Zend_Form_Element_Text('zip');
		$objZip	
	        ->setRequired(true)
			->setAttrib('class','validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_ZIP'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objNeighborhood = new Zend_Form_Element_Text('neighborhood');
		$objNeighborhood
	        ->setRequired(false)
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_NEIGHBORHOOD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objCommunityName = new Zend_Form_Element_Text('community_name');
		$objCommunityName
	        ->setRequired(false)
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_COMMUNITY_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		

		$objLat = new Zend_Form_Element_Text('lat');
		$objLat
	        ->setRequired(false)
			//->setAttrib('class','medium')
			->setAttrib('id','lat')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_NEIGHBORHOOD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objLng = new Zend_Form_Element_Text('lng');
		$objLng
	        ->setRequired(false)
			//->setAttrib('class','medium')
			->setAttrib('id','lng')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP1_NEIGHBORHOOD'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		
		$objStreetView = new Zend_Form_Element_Select('street_view');
		$objStreetView
			->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrLandlordType)
			->setValue('1');
		


		$objAddButton = new Zend_Form_Element_Button('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP1_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success btngeocode')			
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
					
		$objEditButton = new Zend_Form_Element_Button('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP1_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success btngeocode')			
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													


		$this->clearElements();	
		
		$this->addElements(array($objName,$objEmail,$objPhone,$objAddress,$objUnit,$objCity,$objState,$objZip,$objNeighborhood,$objCommunityName,$objLat,$objLng, $objStreetView,$objAddButton,$objEditButton));
	
	
	
	}
	
	
	
	//Landlordstep2 form
	
 public function Landlordstep2()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		global $arrProperty_Type;
		global $arrLot_Size;
		global $arrLandlordType;
		global $arrBedrooms;
		global $arrBathrooms;
		$this->setMethod('post');
		
		
		
		$objRent = new Zend_Form_Element_Text('rent');
		$objRent
	        ->setRequired(true)
			->setAttrib('class','form-textbox validate[required,custom[integer],min[123]]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP2_RENT'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objDeposit = new Zend_Form_Element_Text('deposit');
		$objDeposit
			->setAttrib('class','form-textbox validate[required,custom[onlyNumberSp]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objDepositNegotiable = new Zend_Form_Element_Radio('deposit_negotiable');
		$objDepositNegotiable
			->setAttrib('class','form-textarea-large')
			->setAttrib('id', 'optionsRadios1')						
			->addMultiOptions($arrLandlordType)
			->setValue('0');																						


		$objHighlights = new Zend_Form_Element_Textarea('highlights');
		$objHighlights
			->setAttrib('class','form-textarea-large')
			->addFilter('StripTags')
			//->setAttrib('rows', '5')
			//->setAttrib('cols', '30')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('Label');



		$objBedrooms = new Zend_Form_Element_Select('bedrooms');
		$objBedrooms
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP2_BEDROOMS'))))			
			->addMultiOptions($arrBedrooms);

		$objBathrooms = new Zend_Form_Element_Select('bathrooms');
		$objBathrooms
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required] validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP2_BEDROOMS'))))						
     		->addMultiOptions($arrBathrooms);


		$objYearBuilt = new Zend_Form_Element_Text('year_built');
		$objYearBuilt
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objDateAvailable = new Zend_Form_Element_Text('date_available');
		$objDateAvailable	
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objDescription = new Zend_Form_Element_Textarea('description');
		$objDescription
			->setAttrib('class','form-textarea-large')
			->addFilter('StripTags')
			//->setAttrib('rows', '5')
			//->setAttrib('cols', '30')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('Label');

		$objCommunityName = new Zend_Form_Element_Text('community_name');
		$objCommunityName
			->setAttrib('class','medium')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		$objPropertyType = new Zend_Form_Element_Select('property_type');
		$objPropertyType
	        ->setRequired(true)		
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_USER_USERNAME'))))			
     		->addMultiOptions($arrProperty_Type);
	
	
		$objSquareFootage = new Zend_Form_Element_Text('square_footage');
		$objSquareFootage
			->setAttrib('class','medium validate[required,custom[onlyNumberSp]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');



		$objLotSize = new Zend_Form_Element_Select('lot_size');
		$objLotSize
			->setAttrib('class','box-mini-field')
     		->addMultiOptions($arrLot_Size);
				
		
		$objFiftyFiveOnly = new Zend_Form_Element_Select('fifty_five_only');
		$objFiftyFiveOnly
			->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrLandlordType);


		$objPetPolicy = new Zend_Form_Element_Select('pet_policy');
		$objPetPolicy
			->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrLandlordType);


		$objAmokingAllowed = new Zend_Form_Element_Select('smoking_allowed');
		$objAmokingAllowed
			->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrLandlordType);

	
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP1_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP2_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													
					

		$this->clearElements();	
		
		$this->addElements(array($objRent,$objDeposit,$objDepositNegotiable,$objHighlights,$objBedrooms,$objBathrooms,$objYearBuilt,$objDateAvailable,$objDescription,$objCommunityName,$objPropertyType,$objSquareFootage,$objLotSize,$objFiftyFiveOnly,$objPetPolicy,$objAmokingAllowed,$objAddButton,$objEditButton));
	
	
	
	}


//Landlordstep3 form

 public function Landlordstep3()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		global $arrLaundryType;
		global $arrHeatingType;
		global $arrPatioType;
		global $arrParking_Type;
		global $arrPatioType;
		global $arrHeatingFuelType;
		global $arrCityWaterType;
		global $arrWaterType;
		global $arrSewerType;
		global $arrCoolingType;
		global $arrUtilitiesType;
		
		
		$this->setMethod('post');
	
	//indoor section
	
		$objIndoorCeilingFans = new Zend_Form_Element_Radio('indoor_ceiling_fans');
		$objIndoorCeilingFans
			->setAttrib('class','')
			->setAttrib('id', 'indoor_ceiling_fans')
			->addMultiOptions($arrUtilitiesType);
			//->setValue('0');									


		$objIndoorFurnished = new Zend_Form_Element_Radio('indoor_furnished');
		$objIndoorFurnished
			->setAttrib('class','')
			->setAttrib('id', 'indoor_furnished')						
			->addMultiOptions($arrUtilitiesType);
			//->setValue('');									


		$objIndoorFireplace = new Zend_Form_Element_Radio('indoor_fireplace');
		$objIndoorFireplace
			->setAttrib('class','')
			->setAttrib('id', 'indoor_fireplace')						
			->addMultiOptions($arrUtilitiesType);
			//->setValue('');									


		$objIndoorCableIncluded = new Zend_Form_Element_Radio('indoor_cable_included');
		$objIndoorCableIncluded
			->setAttrib('class','')
			->setAttrib('id', 'indoor_cable_included')						
			->addMultiOptions($arrUtilitiesType);
			//->setValue('');									


		$objIndoorSecuritySystem = new Zend_Form_Element_Radio('indoor_security_system');
		$objIndoorSecuritySystem
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'indoor_security_system')						
			->addMultiOptions($arrUtilitiesType);
			//->setValue('');									

		$objIndoorLaundryType = new Zend_Form_Element_Select('indoor_laundry_type');
		$objIndoorLaundryType
			//->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrLaundryType);
			

		$objIndoorHeatingType = new Zend_Form_Element_Select('indoor_heating_type');
		$objIndoorHeatingType
			//->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrHeatingType);
	
	
	
	//kitchen section
	
		$objKitchenDishwasher = new Zend_Form_Element_Radio('kitchen_dishwasher');
		$objKitchenDishwasher
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'kitchen_dishwasher')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									
	 

		$objKitchenStove = new Zend_Form_Element_Radio('kitchen_stove');
		$objKitchenStove
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'kitchen_stove')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									
	 

		$objKitchenGarbageDisposal = new Zend_Form_Element_Radio('kitchen_garbage_disposal');
		$objKitchenGarbageDisposal
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'kitchen_garbage_disposal')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									


		$objKitchenRefrigerator = new Zend_Form_Element_Radio('kitchen_refrigerator');
		$objKitchenRefrigerator
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'kitchen_refrigerator')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									


		$objKitchenMicrowave = new Zend_Form_Element_Radio('kitchen_microwave');
		$objKitchenMicrowave
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'kitchen_microwave')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									
			
			
	//outdoor section	

		$objOutdoorSwimmingPool = new Zend_Form_Element_Radio('outdoor_swimming_pool');
		$objOutdoorSwimmingPool
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'outdoor_swimming_pool')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objOutdoorGatedCommunity = new Zend_Form_Element_Radio('outdoor_gated_community');
		$objOutdoorGatedCommunity
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'outdoor_gated_community')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objOutdoorLawnCareIncluded = new Zend_Form_Element_Radio('outdoor_lawn_care_included');
		$objOutdoorLawnCareIncluded
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'outdoor_lawn_care_included')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objOutdoorTrashRemovalIncluded = new Zend_Form_Element_Radio('outdoor_trash_removal_included');
		$objOutdoorTrashRemovalIncluded
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'outdoor_trash_removal_included')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objOutdoorFencedYard = new Zend_Form_Element_Radio('outdoor_fenced_yard');
		$objOutdoorFencedYard
	       // ->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'outdoor_fenced_yard')						
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									
			
			
		$objOutdoorParkingType = new Zend_Form_Element_Select('outdoor_parking_type');
		$objOutdoorParkingType
			//->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrParking_Type);
																							
		$objOutdoorPatio = new Zend_Form_Element_Select('outdoor_patio');
		$objOutdoorPatio
			//->setRequired(true)			
			->setAttrib('class','form-selectbox')			
			->addMultiOptions($arrPatioType);

	
	//utilities section	

		$objUtilitiesPestControl  = new Zend_Form_Element_Radio('utilities_pest_control');
		$objUtilitiesPestControl
		    //->setRequired(true)
			->setAttrib('class','')
			->setAttrib('id', 'utilities_pest_control')	
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									


		$objUtilitiesElectric  = new Zend_Form_Element_Radio('utilities_electric');
		$objUtilitiesElectric
		   //->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_electric')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_ELECTRIC'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objUtilitiesHeatingFuel = new Zend_Form_Element_Radio('utilities_heating_fuel');
		$objUtilitiesHeatingFuel
		   // ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_heating_fuel')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HEATING_FUEL'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									

		$objUtilitiesHeatingFuelRent = new Zend_Form_Element_Select('utilities_heating_fuel_rent');
		$objUtilitiesHeatingFuelRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HEATING_FUEL_RENT'))))						
			->addMultiOptions($arrHeatingFuelType);
			

		$objUtilitiesWater = new Zend_Form_Element_Radio('utilities_water');
		$objUtilitiesWater
		   // ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_water')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_WATER'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									


		$objUtilitiesWaterRent = new Zend_Form_Element_Select('utilities_water_rent');
		$objUtilitiesWaterRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_WATER_RENT'))))						
			->addMultiOptions($arrCityWaterType);
			

		$objUtilitiesHotWaterr = new Zend_Form_Element_Radio('utilities_hot_water');
		$objUtilitiesHotWaterr
		   // ->setRequired(true)
			->setAttrib('class','box-mini-field validate[required]')
			->setAttrib('id', 'utilities_hot_water')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HOT_WATER'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');																						
												


		$objUtilitiesHotWaterrRent = new Zend_Form_Element_Select('utilities_hot_water_rent');
		$objUtilitiesHotWaterrRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_HOT_WATER_RENT'))))						
			->addMultiOptions($arrWaterType);


		$objUtilitiesCookingFuel  = new Zend_Form_Element_Radio('utilities_cooking_fuel');
		$objUtilitiesCookingFuel
		    //->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_cooking_fuel')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOKING_FUEL'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									


		$objUtilitiesCookingFuelRent = new Zend_Form_Element_Select('utilities_cooking_fuel_rent');
		$objUtilitiesCookingFuelRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOKING_FUEL_RENT'))))						
			->addMultiOptions($arrWaterType);



		$objUtilitiesSewer  = new Zend_Form_Element_Radio('utilities_sewer');
		$objUtilitiesSewer
		   // ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_sewer')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_SEWER'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									



		$objUtilitiesSewerRent = new Zend_Form_Element_Select('utilities_sewer_rent');
		$objUtilitiesSewerRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_SEWER_RENT'))))						
			->addMultiOptions($arrSewerType);
			
			
		$objUtilitiesCooling = new Zend_Form_Element_Radio('utilities_cooling');
		$objUtilitiesCooling
		   // ->setRequired(true)
			->setAttrib('class','form-selectbox validate[required]')
			->setAttrib('id', 'utilities_cooling')						
			//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOLING'))))
			->addMultiOptions($arrUtilitiesType)
			->setValue(' ');									
		

		$objUtilitiesCoolingRent = new Zend_Form_Element_Select('utilities_cooling_rent');
		$objUtilitiesCoolingRent
			->setRequired(true)			
			->setAttrib('class','box-mini-field validate[required]')			
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP3_UTILITIES_COOLING_RENT'))))
			->addMultiOptions($arrCoolingType);

		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP3_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP3_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													


	$this->clearElements();		
	
			$this->addElements(array($objIndoorCeilingFans,$objIndoorFurnished,$objIndoorFireplace,$objIndoorCableIncluded,$objIndoorSecuritySystem,$objIndoorLaundryType,$objIndoorHeatingType,$objKitchenDishwasher,$objKitchenStove,$objKitchenGarbageDisposal,$objKitchenRefrigerator,$objKitchenMicrowave,$objOutdoorSwimmingPool,$objOutdoorGatedCommunity,$objOutdoorLawnCareIncluded,$objOutdoorTrashRemovalIncluded,$objOutdoorFencedYard,$objOutdoorParkingType,$objOutdoorPatio,$objUtilitiesPestControl,$objUtilitiesElectric,$objUtilitiesHeatingFuel,$objUtilitiesHeatingFuelRent,$objUtilitiesWater,$objUtilitiesWaterRent,$objUtilitiesHotWaterr,$objUtilitiesHotWaterrRent,$objUtilitiesCookingFuel,$objUtilitiesCookingFuelRent,$objUtilitiesSewer,$objUtilitiesSewerRent,$objUtilitiesCooling,$objUtilitiesCoolingRent,$objAddButton,$objEditButton));

	}
	

//Landlordstep4 form
	
  public function Landlordstep4()
	{
		parent::__construct();
		$objTranslate = Zend_Registry::get('Zend_Translate');
		
		$this->setMethod('post');

	
		$objUrl = new Zend_Form_Element_File('url');
		$objUrl	
		//->setRequired(true)		
	    //->setAttrib('class', 'form-textbox validate[required]')
		->addFilter('StripTags')
		->addFilter('StringTrim')
		->addValidator('IsImage')
		->addValidator('Extension', false, array('jpg', 'jpeg', 'png', 'gif'))
		//->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_LANDLORDSTEP4_URL'))))
		->removeDecorator('DtDdWrapper')
		->removeDecorator('Label')
		->addDecorator('PSWrapper');		
		

		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP4_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');


		$objEditButton = new Zend_Form_Element_Submit('edit_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_LANDLORDSTEP4_EDIT'));
		$objEditButton
            ->setAttrib('id', 'edit_btn')
            ->setAttrib('class', 'btn btn-success')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');													

	
		$this->clearElements();	
		
		
		$this->addElements(array($objUrl,$objAddButton,$objEditButton));

	}
	
	
	
}
