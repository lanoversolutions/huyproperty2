<?php
class Models_Form_Contact extends PS_Form
{	
	
	
	public function __construct()
	{
	    parent::__construct();
        $objTranslate = Zend_Registry::get('Zend_Translate');              
        		
		        		
		$objName = new Zend_Form_Element_Text('name');
		$objName
	        ->setRequired(true)
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_NAME'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
		
		
		$objEmail = new Zend_Form_Element_Text('email');
		$objEmail
	        ->setRequired(true)
	        ->setAttrib('class','form-textbox validate[required,custom[email]]')
			->setAttrib('size','50px')
			->addValidator('EmailAddress',  TRUE  )
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_EMAIL'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');	
		
		$objSubject = new Zend_Form_Element_Text('subject');
		$objSubject
	        ->setRequired(true)			
			->setAttrib('class','medium validate[required]')
			->setAttrib('size','50px')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_SUBJECT'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
																						
		$objBody = new Zend_Form_Element_Textarea('body');
		$objBody
	        ->setRequired(true)
			->setAttrib('class','form-textarea-large validate[required]')
			->setAttrib('cols','58')
			->setAttrib('rows','5')
			->addFilter('StripTags')
			->addFilter('StringTrim')
			->addValidator('NotEmpty', true, array('messages' => array('isEmpty' => $objTranslate->_('ADMIN_MSG_INVALID_CONTACT_BODY'))))
			->removeDecorator('Errors')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');
			
		 $objcaptcha = new Zend_Form_Element_Captcha('captcha', array(
			'captcha' => array(
			'captcha' => 'Image',
			'wordLen' => 6,
			'timeout' => 300,
			'width'   => 220,
			'height'  => 80,
			'imgUrl'  => CAPTCHA_PATH,
			'imgDir'  => SERVER_ROOT_PATH . 'captcha',
			'font'    => SERVER_ROOT_PATH . 'styles/font/LiberationSans-Regular.ttf',
			)
			));
		
	

				
		$objAddButton = new Zend_Form_Element_Submit('add_btn',$objTranslate->_('ADMIN_BUTTON_LABEL_CONTACT_ADD'));
		$objAddButton
            ->setAttrib('id', 'add_btn')
            ->setAttrib('class', 'btn')
            ->addFilter('StripTags')
		    ->addFilter('StringTrim')
			->removeDecorator('DtDdWrapper')
			->removeDecorator('Label')
			->addDecorator('PSWrapper');

		
		$this->addElements(array( $objName, $objcaptcha ,$objEmail ,$objSubject ,$objBody ,$objAddButton));
				
	}	


	
}
?>
