<?php
class Models_UserLogin extends Zend_Db_Table_Abstract
{
    protected $_name    = 'front_user';
    protected $_adapter;
    protected $_auth;

    public function verifyLoginInfo($data) {
        
		$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);

    	$dbAdapter = Zend_Registry::get(PS_App_DbAdapter);

        $this->_adapter = new Zend_Auth_Adapter_DbTable($dbAdapter);
        $this->_adapter->setTableName('front_user');
        $this->_adapter->setIdentityColumn('email');
        $this->_adapter->setCredentialColumn('password');
		$this->_adapter->setCredentialTreatment("md5(?) AND user_status=1");
				
		//set the formData
		//$data['user_status']=1;
		$this->_adapter->setIdentity($data['email']);
        $this->_adapter->setCredential($data['password']);
		
        // get Zend_Auth object
        $this->_auth = Zend_Auth::getInstance();
		
        //now authenticate it
        $result = $this->_auth->authenticate($this->_adapter);
		//_pr( $this->_adapter);
		//_pr($result,1);
        if($result->isValid()) {
			
			
            return true;
        }
		
        return false;
    }

    public function setSession() {       			
		
		$objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
		$objError = new Zend_Session_Namespace(PS_App_Error);

        $objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
        $storage = new Zend_Auth_Storage_Session();

        $arrData = array('user_id','email','user_option','user_status','first_name','last_name');		
        $objRow = $this->_adapter->getResultRowObject($arrData);		
        $storage->write($objRow);
		
        $objUser = $this->_auth->getIdentity();		
		
		$objSess->user_id = $objUser->user_id;
		$objSess->first_name = $objUser->first_name;
		$objSess->last_name = $objUser->last_name;
        $objSess->email = $objUser->email;
		$objSess->user_option = $objUser->user_option;
		$objSess->user_status = $objUser->user_status;
		$objSess->isFrontUserLogin  = true;
		
		$objError->message = $objTranslate->translate('LOGIN_SUCCESS');
	}

    public function clearSession() {
        $objTranslate = Zend_Registry::get(PS_App_Zend_Translate);
	
        Zend_Auth::getInstance()->clearIdentity();

	    $objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		$objSess->user_id       = "";
		$objSess->email   	= "";
		$objSess->first_name 	= "";
		$objSess->user_option    = "";
		$objSess->first_name    = "";
		$objSess->last_name    = "";
		$objSess->isFrontUserLogin  = false;
		$objSess->__unset(PS_Front_App_Auth);
		$objError = new Zend_Session_Namespace(PS_App_Error);
		$objError->message = $objTranslate->translate('LOGOUT_SUCCESS');
		$objError->messageType = 'confirm';
    }
			
   public function save(array $data)
    {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) {
            if (!in_array($field, $fields)) {
                unset($data[$field]);
            }
        }
        $data['dcreated'] = date('Y-m-d H:i:s');
        return $this->insert($data);
    }

    public function fetchEntries()
    {
        return $this->fetchAll('1')->toArray();
    }

    public function fetchEntry($user_id)
    {
        $select = $this->select()->where('user_id = ?', $user_id);
        return $this->fetchRow($select)->toArray();
    }
   
     public function checkemail($email)
    {
    	//echo "Hello";exit;
    	$select = $this->select();
		$select->where("email = '".$email."' AND user_status = '1'");		
		if( $this->fetchRow($select) )
			return $this->fetchRow($select)->toArray();
		else
			return false;
    }


/*----------------------| Update new password  |----------------------*/

	public function SetNewPasswrod(array $data,$user_id) 
	{ 
		$where = "user_id = " . $user_id;
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
		
		if(isset($data['password']) && $data['password'] != "") {
			$data['password'] = md5($data['password']);
		}
																	
		$data['dmodified'] = date('Y-m-d H:i:s');
								
		return $this->update($data,$where);
    }
	
	/**
     * Fetch Particular  user_id Detail
     *	 
     */
    /*----------------------| Get spacific user_id Detail   |----------------------*/
	
	public function fetchuserid($user_id){
		
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('f'=>'front_user'),array('f.user_id','f.user_option','f.*'))								
				->where('f.user_id = ?', $user_id);
	   
	    
		$select = $this->fetchRow($select);
		$select = $select->toArray();    									
				
		if($select)
	    	return $select;
		else
      		return null;    
    }

}


