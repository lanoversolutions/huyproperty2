<?php

/**
 * Class for Tenantregister table related database operations.
 *
 * @category   PS
 * @package    Models_User
 * @copyright  Copyright (c) 2010 - 2012 
 */

class Models_Tenant extends PS_Database_Table
{  
    /**#@+0
     * @access protected
     */

    /**
     * The table name.
     *
     * @var array|string
     */
	protected $_name = 'property'; 		
                   
  	   	/**
     * Get all tenant listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function getList($searchText='', $rent='', $square_footage_min='', $square_footage_max='',$property_type='',$bedrooms='',$bathrooms='',$sortby='')
	{
	//echo $square_footage_min;
	//echo 'AND';	 	
	//echo $square_footage_max;exit; 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $rent = addslashes($rent);
	 $square_footage_min = addslashes($square_footage_min);
	 $square_footage_max = addslashes($square_footage_max);
	 $property_type = $property_type;
	 $bedrooms = addslashes($bedrooms);
	 $bedrooms = ($bedrooms>0)? $bedrooms:0;
	 $bathrooms = addslashes($bathrooms);
	 $bathrooms = ($bathrooms>0)? $bathrooms:0;
	 
	 if($square_footage_max == 0){
	 		$cond_string = "(s.name like '%".$search_txt."%' OR p.city like '%".$search_txt."%' OR p.zip like '%".$search_txt."%') AND p.square_footage >=".$square_footage_min." AND p.bedrooms >= ".$bedrooms." AND p.bathrooms >= ".$bathrooms."";
	 }else{
	 		$cond_string = "(s.name like '%".$search_txt."%' OR p.city like '%".$search_txt."%' OR p.zip like '%".$search_txt."%') AND p.square_footage >=".$square_footage_min." AND  p.square_footage <=".$square_footage_max." AND p.bedrooms >= ".$bedrooms." AND p.bathrooms >= ".$bathrooms."";
	 }
	 
	 
	
	if( isset($rent) && $rent != ''){
	 	 $cond_string .= " AND (p.rent >=0 AND p.rent <= ".$rent.")";
	 } 
	 
	 if( isset($property_type) && $property_type != ''){
	 	 $cond_string .= " AND p.property_type IN (".$property_type.")";
	 }
	 
	 	 	
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('p'=>'property'),array('p.*'))
				->joinleft(array('pp'=>'property_photo'), 'p.id  = pp.property_id', array('pp.url'))
				->joinleft(array('s'=>'states'), 'p.state_id = s.id', array('s.id as states_id','s.statecode'))
				->where('p.property_status = 1');
					
		

	   //echo $select;exit; 
		if($search_txt != '' || $rent != '' || $square_footage_min != '' || $square_footage_max != '' || $property_type != '' || $bedrooms != '' || $bathrooms != '' )
			$select->where($cond_string);
			
		$select->group("p.id");
		
		if(trim($sortby)=='')
			$select->order('p.created DESC');
			       //->limit(0,5);
		else
			$select->order($strSort);
		 			
		//echo $select;exit; 										    	    	    	   		 
		
	 return $select;	 	    				
  	}



  	   	/**
     * Get all tenant listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
	public function gettenantList($state_id='', $county='', $bedrooms='',$sortby='')
	{
	//echo $square_footage_min;
	 $state_id = addslashes($state_id);
	 $county = addslashes($county);
	 $bedrooms = addslashes($bedrooms);
	 

	 $cond_string = "t.state_id like '%".$state_id."%' AND t.county like '%".$county."%' AND t.bedrooms like '%".$bedrooms."%'";
	
	 	 	
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('t'=>'tenant'),array('t.*'))
				->joinleft(array('f'=>'front_user'), 't.user_id = f.user_id', array('f.email'));
		
	   //echo $select;exit; 
		if($state_id != '' || $county != '' || $bedrooms != '')
			$select->where($cond_string);
		
		if(trim($sortby)=='')
			$select->order('t.id DESC');
			       //->limit(0,5);
		else
			$select->order($strSort);
		 			
			//echo $select;exit; 										    	    	    	   		 
		
	 return $select;	 	    				
  	}

	
  	   	/**
     * Get all tenant listing.    
    * $sortby sorts in asc or desc order
     * @return array 
     */    	
	/*----------------------| fetch all data and searching parameters |---------------*/		
/*	public function getList($searchText='',$sortby='')
	{ 	 										    	    	    	   		 
	 $search_txt = addslashes($searchText);
	 $cond_string = "s.".'name'." like '%".$search_txt."%'";		 	 
	 	
			 
	 $select = $this->select();
	 $select->setIntegrityCheck(false)	 			
				->from(array('p'=>'property'),array('p.id as pid','p.*'))
				->join(array('s'=>'states'), 'p.state_id = s.id', array('s.id','s.name as sttatename'));
	
	   //echo $select;exit;	
		if($search_txt != '' )
			$select->where($cond_string);

		if(trim($sortby)=='')
			$select->order('p.id ASC');
		else
			$select->order($strSort);
	
	//echo $select; exit;	 	 			
			
	 return $select;	 	    				
  	}
*/	
	
	
	 /**
     * Fetch All image
     *	 
     */
    /*----------------------| Get All property photo Detail   |----------------------*/
    public function fetchimage($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('pp'=>'property_photo'),array('pp.*'))								
				->where('pp.property_id = '.$id);
				
    	
		$select = $this->fetchAll($select);
		
		if($select)
			return $select = $select->toArray();
			
		else
      		return null;    
    }
	

	 /**
     * Fetch Particular Tenantregister Detail
     *	 
     */
    /*----------------------| Get spacific Tenantregister Detail   |----------------------*/
    public function fetchTenant($id) {
		$objTranslate = Zend_Registry::get('Zend_Translate');
		$select = $this->select();
		$select->setIntegrityCheck(false)
				->from(array('t'=>'tenant'),array('t.*'))								
				->where('t.user_id = '.$id);
		
		$select = $this->fetchRow($select);
		
		if($select)
			return $select = $select->toArray();
		else
      		return null;    
    }


	
	
	/**
    * Save data for new Tenantregister entry.
    *
    * @param  array $data Array of Tenantregister data to insert in database.
    * @return integer|boolean Last inserted author id
    */
	/*----------------------| Insert data into table  |-----------------------------*/		
	public function saveData(array $data) {
        $fields = $this->info(Zend_Db_Table_Abstract::COLS);
        foreach ($data as $field => $value) 
        {
            if (!in_array($field, $fields)) 
            {
                unset($data[$field]);
            }
        }   
		
		$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		$data['dcreated'] = date('Y-m-d H:i:s');                        
        
        return $this->insert($data);
     }
	
	

    /**
     * Update data for Tenantregister entry.
     *
     * @param  array $data Array of Tenantregister data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateData(array $data,$id='') { 
		$where = "user_id = '".$id."'";
		
		//$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																					
		return $this->update($data,$where);
    }



	    /**
     * Update data for Tenantregister entry.
     *
     * @param  array $data Array of Tenantregister data to update in database.
     * @param  integer $id User id for which you want to update data.
     * @return integer|boolean Last inserted page id
     */         
	/*----------------------| update data  |-----------------------------*/   
	public function updateTenant(array $data,$id) { 
		$where = " id = '".$id."'";
		
		$data['sometime_after'] = date('Y-m-d' , strtotime($data['sometime_after']));
		$data['dmodified'] = date('Y-m-d H:i:s');  
		
		$fields = $this->info(Zend_Db_Table_Abstract::COLS);
		foreach ($data as $field => $value) {
			if (!in_array($field, $fields)) {
				unset($data[$field]);
			}
		}
																					
		return $this->update($data,$where);
    }



		/**
     * Delete data forom Tenant entry.
     *     
     */         
	/*----------------------| Delete data  |-----------------------------*/ 
	public function deleteTenantData($id) {									
		$objData = $this->fetchRow('id='.$id);		
		if($objData)
			$objData->delete();
		else
      		return null;    
		unset($objData);
	}
	
			 	      	
}
?>
