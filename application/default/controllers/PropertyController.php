<?php

/**
 * PropertyController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class PropertyController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
		$data = array();
		$data['id']= $objRequest->id;
		$data['actionName']= $actionName;
		$genaral_data = $this->view->partial('genaral.phtml' ,array('data'=>$data));
		
		$this->view->genaral_data = $genaral_data;
		
	}
	
	
	
	/**
	 * The "edit" action is use to edit a Account
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/account
	 *
	 * @return void
	 */
	/*Edit Accountedit Action Start*/
	public function accountAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);

		$objModel = new Models_Frontuser();
		$objForm = new Models_Form_Property();					
		
		
		$id = $objSess->user_id;										
		$arrData = array ();
		$arrData = $objModel->fetchEntry ( $id );																
			
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();			
		
			if ($objForm->isValid ( $formData )) {
					
					//_pr($arrData,1);
					$arrData['user_option'] = $formData['user_option'];
																		
					$objModel->updateData ($arrData,$id);					
					
					//update the session for type
					$arrUserData = $objModel->fetchEntry ($id);
					$objSess->user_option = $arrUserData['user_option'];
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_PROPERTY_INSERT');
					$objError->messageType = 'confirm';
					
					if($formData['user_option']== '1'){
							$this->_redirect ( "/property/landlordstep1add" );
					}else{
						
							$this->_redirect ( "/user/tenantaccount" );
					}
					
			} else {
				$objForm->populate($formData);
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';

			}
						//Auto populate the records
			$objForm->populate ( $arrData );
			
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'account';
		$this->view->objForm = $objForm;
		$this->view->id = $id;	
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );			
	}
	/*Edit Pagesedit Action End*/
	


	/**
	 * The "add" action is use to add a landlordstep1add
	 *
	 * This action to use the add data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep1add
	 *
	 * @return void
	 */
	/*Add landlordstep1add  Action Start*/
	public function landlordstep1addAction() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORDSTEP1_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$objModel = new Models_Property ();
		$objForm = new Models_Form_Property ();
		
		//create the Landlordstep1 form object.
		$objForm->Landlordstep1();
		
		/* for States combo box */						
		$arrStates = $objModel->getStateCombobox();
		$objForm->state_id->addMultiOptions($arrStates);														
		 /*combo box end */
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			
			if ($objForm->isValid ( $formData )) {
					
					//insert the userid in Propertytable							
					$formData['user_id'] = $objSess->user_id;	

					if(isset($formData['address_lat']) && ($formData['address_lng']) ){
						$formData['address_status'] = 1;
					}else{
						$formData['address_status'] = 0;
					}
					
					//Save Formdata												
					$id = $objModel->saveData ( $formData );
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/property/landlordstep2/id/".$id);
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Step1Add';
		$this->view->objForm = $objForm;
	}
	/*Add Action End*/
	


	/**
	 * The "edit" action is use to edit a landlordstep1
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep1
	 *
	 * @return void
	 */
	/*Edit landlordstep1edit Action Start*/
	public function landlordstep1Action() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORDSTEP1_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$id = $objRequest->id;
		
		/*Check authorization access*/
		if(empty($id)){
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		$objModel = new Models_Property ();
		$arrData = $objModel->fetchEntry( $id ,$objSess->user_id );
		if(!empty($arrData)){
			if($arrData['user_id'] != $objSess->user_id ){
				$this->_redirect ( "/property/error");
			}
		}else{
			$this->_redirect ( "/property/landlordstep1add");
		}
		/*End Authorization*/
		

		$objForm = new Models_Form_Property ();	
		
		//create the Landlordstep1 form object.
		 $objForm->Landlordstep1();
		 
		 		/* for States combo box */						
		$arrStates = $objModel->getStateCombobox();
		$objForm->state_id->addMultiOptions($arrStates);														
		 /*combo box end */

		 	
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			
			if ($objForm->isValid ( $formData )) {
					
					//insert the userid in Propertytable								
					$formData['user_id'] = $objSess->user_id;
					//_pr($formData,1);
					if(isset($formData['address_lat']) && ($formData['address_lng']) ){
						$formData['address_status'] = 1;
					}else{
						$formData['address_status'] = 0;
					}
					//_pr($formData,1);
					//Update Formdata 
					$objModel->updateData ( $formData , $id ,$objSess->user_id);
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/property/landlordstep2/id/".$id);
					
			} else {
				//populate formData
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}else{
			if(count($arrData)>0){
				$objForm->populate ( $arrData );
			}
		}
		$this->view->id= $id;
		
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'step1Edit';
		$this->view->objForm = $objForm;
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );
	}
	/*Add pagesadd Action End*/
	

	/**
	 * The "edit" action is use to edit a landlordstep2
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep2
	 *
	 * @return void
	 */
	/*Edit landlordstep2edit Action Start*/
	public function landlordstep2Action() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORDSTEP2_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$id = $objRequest->id;
		/*Check authorization access*/
		if(empty($id)){
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		$objModel = new Models_Property ();
		$arrData = $objModel->fetchEntry( $id ,$objSess->user_id );
		if(!empty($arrData)){
			if($arrData['user_id'] != $objSess->user_id ){
				$this->_redirect ( "/property/error");
			}
		}else{
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		/*End Authorization*/

		$objForm = new Models_Form_Property ();	
		
		//create the Landlordstep2 form object.
		$objForm->Landlordstep2();
		
		//get the description data
		$description = $arrData['description'];

		//get the highlights data
		$highlights = $arrData['highlights'];


		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			
			if ($objForm->isValid ( $formData )) {
					
					//Insert the userid in Propertytable
					$formData['user_id'] = $objSess->user_id;	
					
					//Update Formdata 
					$objModel->updateData ( $formData ,$id,$objSess->user_id );

					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/property/landlordstep3/id/".$id);
					
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}else{
			if(count($arrData)>0){
				$objForm->populate ( $arrData );		
				//_pr($arrData,1);
			}
		}
			
		$this->view->id= $id;
		$this->view->description = $description;	
		$this->view->highlights = $highlights;			
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Step2Edit';
		$this->view->objForm = $objForm;
		
		unset ( $objForm, $objError, $objModel, $objRequest, $objTranslate );	
	}
	/*Edit Action End*/


	/**
	 * The "edit" action is use to edit a landlordstep3
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep3
	 *
	 * @return void
	 */
	/*Edit landlordstep3edit Action Start*/
	public function landlordstep3Action() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORDSTEP3_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$id = $objRequest->id;
		/*Check authorization access*/
		if(empty($id)){
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		$objModelProperty = new Models_Property ();
		$arrDataProperty = $objModelProperty->fetchEntry( $id ,$objSess->user_id );
		if(!empty($arrDataProperty)){
			if($arrDataProperty['user_id'] != $objSess->user_id ){
				$this->_redirect ( "/property/error");
			}
		}else{
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		/*End Authorization*/
		
		$objModel = new Models_PropertyAmenities ();
		$objForm = new Models_Form_Property ();
		//create the Landlordstep3 form object.
		$objForm->Landlordstep3();

		//fetch the PropertyAmenitiestable data
		$arrData = $objModel->fetchEntry( $id,$objSess->user_id );
		 //_pr($arrData,1);
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			if ($objForm->isValid ( $formData )) {
				 //insert the referenceid in PropertyAmenitiestable			
					$formData['property_id'] = $id;
				 	
					//Update Formdata 
					if(count($arrData)>0){
						$objModel->updateData ( $formData ,$id );
					}else{
						//Save Formdata	
						$objModel->saveData ( $formData );
					}
			
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/property/landlordstep4/id/".$id);
			} else {
					$objForm->populate ( $formData );
					$objError->message = formatErrorMessage ( $objForm->getMessages () );
					$objError->messageType = 'error';
			}
		}else{
			if(count($arrData)>0){
				$objForm->populate ( $arrData );
			}
		}

		$this->view->id= $id;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'step3Edit';
		$this->view->objForm = $objForm;
	}
	/*Edit Action End*/


	/**
	 * The "edit" action is use to edit a landlordstep4
	 *
	 * This action to use the edit data
	 
	 * via the following urls:
	 *
	 * /property/landlordstep4
	 *
	 * @return void
	 */
	/*Edit landlordstep4edit Action Start*/
	public function landlordstep4Action() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORDSTEP4_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);

		$id = $objRequest->id;
		/*Check authorization access*/
		if(empty($id)){
			$this->_redirect ( "/property/landlordstep1add");
		}
		
		$objModelProperty = new Models_Property ();
		$arrDataProperty = $objModelProperty->fetchEntry( $id ,$objSess->user_id );
		if(!empty($arrDataProperty)){
			if($arrDataProperty['user_id'] != $objSess->user_id )
				$this->_redirect ( "/property/error");
		}else{
			$this->_redirect ( "/property/landlordstep1add");
		}
		/*End Authorization*/
		
		$objModel = new Models_PropertyPhoto ();
		$objForm = new Models_Form_Property ();
		//create the Landlordstep4 form object.	
		$objForm->Landlordstep4();
		
		//fetch the PropertyPhototable data
		$arrData = $objModel->fetchEntry( $id,$objSess->user_id );
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();						
			if ($objForm->isValid ( $formData )) {
				//_pr($formData,1);
				$postArrDate = array();
				//insert the referenceid in PropertyPhototable
				$formData['property_id'] = $formData['id'];
				unset($formData['id']);
					if(isset($_FILES['url'])){	
						$photo = new PS_File_Transfer_Adapter_Http();										
						$photo->setDestination(IMAGE_ROOT_PATH);																
						$new_photo = uniqid('url_', FALSE).'.'.pathinfo($photo->getFileName('url') ,PATHINFO_EXTENSION);    					
						$photo->addFilter('Rename', $new_photo);										
						if ($photo->isValid('url')) {
							$photo->receive('url');
							$formData['url'] = pathinfo($photo->getFileName('url') ,PATHINFO_BASENAME);						
						}
					}
					
					//Save Formdata
					$objModel->saveData( $formData  );
					//_pr($formData,1);		
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/property/landlordstep4/id/".$id);
					
			
			} else {
				//populate formData
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'step4Edit';
		$this->view->arrData = $arrData;
		$this->view->id= $id;
		$this->view->objForm = $objForm;
	}
	/*Edit Action End*/
	
	
	/* dashboard Action */
	public function dashboardAction(){
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('FRONT_LABEL_PAGETITLE_DASHBOARD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		$objRequest = $this->getRequest ();
		
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
	}
	
	/**
	 * The "Listing" action is use to display a propertylisting
	 *
	 * This action to use the list data
	 
	 * via the following urls:
	 *
	 * /property/propertylisting
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/
	public function propertylistingAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORD');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Property ();		
		$objForm = new Models_Form_Property ();
		
		$arrStates = $objModel->getStateCombobox();
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getList ( $searchText, $searchType, $sortby , $objSess->user_id );
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	/*Listing Action End*/
	

	/**
	 * The "Propertydetails" action is use to display a propertydetails
	 *
	 * This action to use the display property details.
	 
	 * via the following urls:
	 *
	 * /property/propertylisting/propertydetails
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/

	public function propertydetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Property ();
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );
		$arrStates = $objModel->getStateCombobox();
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'propertylisting';
		unset ($objModel, $objRequest, $objTranslate );
					
	}



	
	/**
	 * The "delete" action is use to delete a data
	 *
	 *	This action to use the delete data
	 
	 * via the following urls:
	 *
	 * /property/propertylisting
	 *
	 * @return void
	 */
	/*Delete propertydelete Action Start*/
	public function deleteAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);
		
		$objModel = new Models_Property ();						
		
		$id = $objRequest->id;						
		$objModel->deleteData($id,$objSess->user_id);	

		
		$objModelPropertyAminities = new Models_PropertyAmenities ();
		$objModelPropertyAminities->deleteData($id);	
		
		$objModelPropertyPhoto = new Models_PropertyPhoto ();
		$id = $objRequest->id;						
		$arrData = array ();
		$arrData = $objModelPropertyPhoto->fetchimages ( $id );
		foreach ( $arrData as $data ):
		$DeletePath = IMAGE_ROOT_PATH.$data['url'];
		unlink($DeletePath);
		endforeach;
		$objModelPropertyPhoto->deleteData($id);	
		
		$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_IMAGE_DELETE');
		$objError->messageType = 'confirm';		
		$this->_redirect ( "/property/propertylisting" );				
		exit;
	}
	/*Delete  Action End*/
	
	
	
	
	/**
	 * The "delete" action is use to delete a image
	 *
	 *	This action to use the delete image
	 * via the following urls:
	 *
	 * /property/landlordstep4
	 *
	 * @return void
	 */
	/*Delete imagedelete Action Start*/
	public function deleteimageAction() {
	
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objModel = new Models_PropertyPhoto();						
		
		$id = $objRequest->id;						
		$arrData = array ();
		$arrData = $objModel->fetchimage ( $id );
		
	    $dirImageRootPath= IMAGE_ROOT_PATH;

		$file = $dirImageRootPath.$arrData['url'];
		
		unlink($file);
		
		if(empty($arrData['url']) && empty($arrData['url']))
			rmdir($dirImageRootPath.$id);
		
		$objModel->deleteimages ( $id );
												
		$objError->message = $objTranslate->translate('ADMIN_MSG_SUCCESS_IMAGE_DELETED');
		$objError->messageType = 'confirm';		
		$this->_redirect ( "/property/landlordstep4/id/".$arrData['property_id']);				
		exit;
	}
	/*Delete Action End*/
	
	
	/* dashboard Action */
	public function errorAction(){
	
	}

	
}
?>
