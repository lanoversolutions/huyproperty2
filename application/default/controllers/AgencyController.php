<?php

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class AgencyController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;	
		$this->view->controllerName = $controllerName;
	
			//Google Rightsideadsense
		$rightsideadsense = $this->view->partial('rightsideadsense.phtml' ,array());
		$this->view->rightsideadsense = $rightsideadsense;

	    //Google Rightsideadsense
		$rightsidetextadsense = $this->view->partial('rightsidetextadsense.phtml' ,array());
		$this->view->rightsidetextadsense = $rightsidetextadsense;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;

	}
	
	
	
		/* dashboard Action */
	public function indexAction(){
	
	    $objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );

		//define meta tags
		$this->view->siteTitle = $objTranslate->translate('ADMIN_PAGETITLE_AGENCY_INDEX');
		$this->view->headMeta()->setName($objTranslate->translate('KEYWORD'), $objTranslate->translate('ADMIN_PAGETITLE_AGENCY_KEYWORD')); 
		$this->view->headMeta()->setName($objTranslate->translate('DESCRIPTION'), $objTranslate->translate('ADMIN_PAGETITLE_AGENCY_DESCRIPTION'));
		
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();	
		
		
		
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->currentAction = 'index';
	}
	


			/**
	 * The "states" action is use to display a agencies
	 *
	 * This action to use the list data
	 
	 * via the following urls:
	 *
	 * /agency/states
	 *
	 * @return void
	 */
	 /* states Action Start*/
	public function stateAction(){	
			
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		//$this->view->siteTitle = 'Apply for Housing Choice Voucher in '.$objRequest->filename.' -  Section 8 Housing';
		
	
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		
		
		$objModel = new Models_Agency ();
		
		$filename = $objRequest->filename;
		$arrData = array ();
		$arrData = $objModel->fetchstates( $filename );
		//_pr($arrData,1);
		
		//define meta tags
		$ADMIN_LABEL_HEADING_STATE = str_replace('%%state%%',$arrData[0]['sname'],$objTranslate->translate('ADMIN_LABEL_HEADING_STATE'));
		$this->view->siteTitle = $ADMIN_LABEL_HEADING_STATE;
		$this->view->headMeta()->setName($objTranslate->translate('KEYWORD'), $objTranslate->translate('ADMIN_PAGETITLE_STATE_KEYWORD')); 
		$this->view->headMeta()->setName($objTranslate->translate('DESCRIPTION'), $objTranslate->translate('ADMIN_PAGETITLE_STATE_DESCRIPTION'));
		
		
		$this->view->arrDataList = $arrData;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->currentAction = 'states';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	/*Listing Action End*/
		
	
	
	
	
}
?>
