<?php

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class TenantController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
	}
	
	
	/**
	 * The "Listing" action is use to display a propertylisting
	 *
	 * This action to use the list data
	 
	 * via the following urls:
	 *
	 * /Tenant/propertylisting
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/
	public function indexAction(){			
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Tenant ();		
		$objForm = new Models_Form_Tenant ();
		$objForm->Listsearch();	
	    
		global $arrBedrooms;
		global $arrBathrooms;
		global $arrProperty_Type;

		/*$objModelProperty = new Models_Property();
		$arrStates = $objModelProperty ->getStateCombobox();*/
			
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$rent = '';
		$square_footage_min = '';
		$square_footage_max = '';
		$property_type = '';
		$bedrooms = '';
		$bathrooms = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
		    //_pr($formData,1);
			
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['rent']))
					$rent = $formData['rent'];
				
				if(isset($formData['square_footage_min']))
					$square_footage_min = $formData['square_footage_min'];																

				if(isset($formData['square_footage_max']))
					$square_footage_max = $formData['square_footage_max'];																

				if(isset($formData['property_type']) && !empty($formData['property_type']))
					$property_type = implode(",",$formData['property_type']);

				if(isset($formData['bedrooms']))
					$bedrooms = $formData['bedrooms'];

				if(isset($formData['bathrooms']))
					$bathrooms = $formData['bathrooms'];
				
				//echo $property_type; exit;	
				
				if (isset($formData['txtsearch']) && isset($formData['rent']) && isset($formData['square_footage_min']) &&  isset($formData['square_footage_max']) && isset($formData['property_type']) && isset($formData['bedrooms']) && isset($formData['bathrooms'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'rent' => $rent, 'square_footage_min' => $square_footage_min, 'square_footage_max' => $square_footage_max,'property_type' => $property_type,'bedrooms' => $bedrooms ,'bathrooms' => $bathrooms,'sortby' => $sortby );
					
				}
				
			}
			//_pr($formData,1);
			$objForm->populate ( $formData );
			
			
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
			
		//_pr($square_footage,1);
		$objSelect = $objModel->getList( $searchText, $rent, $square_footage_min , $square_footage_max ,$property_type, $bedrooms, $bathrooms, $sortby);

		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		//$this->view->arrStates = $arrStates;
		$this->view->arrBedrooms = $arrBedrooms;
		$this->view->arrBathrooms = $arrBathrooms;
        $this->view->arrProperty_Type = $arrProperty_Type;		
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	/*Listing Action End*/
	
	
	/**
	 * The "Listing" action is use to display a Local Tenants
	 *
	 * This action to use the list data
	 
	 * via the following urls:
	 *
	 * /Tenant/localtenants
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/
	public function localtenantsAction(){			
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Tenant ();		
		$objForm = new Models_Form_Tenant ();
		$objForm->localtenants();	
	    
		$objModelProperty = new Models_Property();
		$arrStates = $objModelProperty ->getStateCombobox();
		$objForm->state_id->addMultiOptions($arrStates);
		
 		global $arrBedroom;
		global $arrProperty_Type;
			
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$state_id = '';
		$county = '';
		$bedrooms = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
		       // _pr($formData,1);
			
			if(isset($formData['search'])){
	
				if(isset($formData['state_id']))
					$state_id = $formData['state_id'];
				
				if(isset($formData['county']))
					$county = $formData['county'];																

				if(isset($formData['bedrooms']))
					$bedrooms = $formData['bedrooms'];																

				
				//echo $property_type; exit;	
				
				if (isset($formData['state_id']) && isset($formData['county']) &&  isset($formData['bedrooms'])) {
					$pagingExtraVar = array ('state_id' => $state_id, 'county' => $county, 'bedrooms' => $bedrooms,'sortby' => $sortby );
					
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
			
		//_pr($square_footage,1);
		$objSelect = $objModel->gettenantList( $state_id , $county ,$bedrooms, $sortby);

		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->arrBedrooms = $arrBedroom;
		$this->view->arrProperty_Type = $arrProperty_Type;
		$this->view->user_id =  $objSess->user_id;
		$this->view->email =  $objSess->email;	
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		$this->view->objForm = $objForm;
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	/*Listing Action End*/


	/**
	 * The "Rentaldetails" action is use to display a property
	 *
	 * This action to use the display property details.
	 
	 * via the following urls:
	 *
	 * /tenant/rentaldetails
	 *
	 * @return void
	 */
	 /* rentaldetails Action Start*/

	public function rentaldetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		//$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_RENTAL');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Tenant ();
		$objForm = new Models_Form_Tenant ();	
		$objForm->tenantcontact();	
		
		
	    $id = $objRequest->id;										
		$arrData = array ();
		$objModelProperty = new Models_Property ();
		$arrData = $objModelProperty->fetchdetails ( $id );
		//_pr($arrData,1);
		
		$arrStates = $objModelProperty->getStateCombobox();
		
		$arrImageData = $objModel->fetchimage ( $id );
		
						
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
							
			if ($objForm->isValid ( $formData )) {
							
							//_pr($formData,1);
											
							$html = new Zend_View();
							$html->setScriptPath(EMAIL_TENANTCONTACT_BODY_PATH);
							
						/* Send activation link to register user in email*/ 
							$from = $formData['first_name']."<".$formData['email'].">";
							$to =  $arrData['email'];
							$mail_subject='Inquiry form customer';
                             						
							$mail_body = $html->render('tenantcontact.phtml');
							$mail_body = str_replace('{$confirmation_firstname}',$formData['first_name'],$mail_body);
							$mail_body = str_replace('{$confirmation_lastname}',$formData['last_name'],$mail_body);
							$mail_body = str_replace('{$confirmation_email}',$formData['email'],$mail_body);
							$mail_body = str_replace('{$confirmation_phone}',$formData['phone'],$mail_body);
							$mail_body = str_replace('{$confirmation_body}',$formData['body'],$mail_body);
							$mail_body = stripslashes($mail_body);
							
							$headers = "MIME-Version: 1.0\r\n";
							$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							$headers .= "From: ".$from."\r\n";
							//echo $from;
							//echo $to;
							//echo $mail_body; exit;
														
							if(isset($this->objSess->user_id)){ 
							//$objSendMail =new Models_SendEmail();
							//$objSendMail->sendEmail($to, $mail_subject, $mail_body, $headers);
							mail($to, $mail_subject, $mail_body, $headers);
							}else{
							$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_PLEASE_LOGIN');	
							$objError->messageType = 'error';
							$this->_redirect ( "/tenant/rentaldetails/id/".$id );
							}
						/* End Activation link*/
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_MAIL_SEND');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/tenant/rentaldetails/id/".$id );
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
		
		
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->user_id =  $objSess->user_id;
		$this->view->arrDataList = $arrData;
		$this->view->arrimageDataList = $arrImageData;
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->objForm = $objForm;
		$this->view->currentAction = 'rentaldetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}

	
	
}
?>
