<?php

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class PageController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
	}
	
	
	/* about Action */
	public function aboutAction(){
	
	}
	
	/* termsconditions Action */
	public function termsconditionsAction(){
	
	}


	/* termsconditions Action */
	public function privacypolicyAction(){
	
	}


	/* termsconditions Action */
	public function helpAction(){
	
	}

	
}
?>
