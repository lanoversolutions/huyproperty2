<?php

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class ContactController extends PS_Controller_FrontAction 
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
	
	    //Google Rightsideadsense
		$rightsidetextadsense = $this->view->partial('rightsidetextadsense.phtml' ,array());
		$this->view->rightsidetextadsense = $rightsidetextadsense;
	
	}
	
	
	
	
	/**
	 * The "index" action is use to add a contact
	 *
	 * This action to use the add data
	 
	 * via the following urls:
	 *
	 * /contact/index
	 *
	 * @return void
	 */
	/*index Action Start*/
	
		public function indexAction() {
			   
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_CONTACT_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Contact ();
		$objForm = new Models_Form_Contact ();	
		
	    				
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
					
			if ($objForm->isValid ( $formData )) {
				
				
					
				    // Save data
				    $retrivedId = $objModel->saveData ( $formData );
																
					if ($retrivedId){
						
							$html = new Zend_View();
							$html->setScriptPath(EMAIL_CONTACT_BODY_PATH);
							
						/* Send activation link to register user in email*/ 
							$from = $formData['name']."<".$formData['email'].">";
							$to =  CONFIGURATION_EMAIL_FROM;
							$mail_subject='Inquiry form customer';
							
							$mail_body = $html->render('contact.phtml');
							$mail_body = str_replace('{$confirmation_name}',$formData['name'],$mail_body);
							$mail_body = str_replace('{$confirmation_email}',$formData['email'],$mail_body);
							$mail_body = str_replace('{$confirmation_subject}',$formData['subject'],$mail_body);
							$mail_body = str_replace('{$confirmation_body}',$formData['body'],$mail_body);
							$mail_body = stripslashes($mail_body);
							
							//echo $mail_body; exit;
							
							$headers = "MIME-Version: 1.0\r\n";
							$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
							$headers .= "From: ".$from."\r\n";
														
							//$objSendMail =new Models_SendEmail();
							//$objSendMail->sendEmail($to, $mail_subject, $mail_body, $headers);
							mail($to, $mail_subject, $mail_body, $headers);
									
						/* End Activation link*/
					}
					
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_CONTACT_INSERT');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/contact/index" );
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Add';
		$this->view->objForm = $objForm;
	}
	/*Add pagesadd Action End*/

	
	
	
	
}
?>
