<?php

/**
 * IndexController is the default controller for this application
 * 
 * Notice that we do not have to require 'Zend/Controller/Action.php', this
 * is because our application is using "autoloading" in the bootstrap.
 *
 * @see http://framework.zend.com/manual/en/zend.loader.html#zend.loader.load.autoload
 */
class IndexController extends PS_Controller_FrontAction
{
	
	function init() {
		parent::init ();
		$objRequest = $this->getRequest ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;
		
		//Google Rightsideadsense
		$rightsideadsense = $this->view->partial('rightsideadsense.phtml' ,array());
		$this->view->rightsideadsense = $rightsideadsense;

		//Google Middlepartadsense
		$middlepartadsense = $this->view->partial('middlepartadsense.phtml' ,array());
		$this->view->middlepartadsense = $middlepartadsense;

		//Google Bottompartadsense
		$bottompartadsense = $this->view->partial('bottompartadsense.phtml' ,array());
		$this->view->bottompartadsense = $bottompartadsense;

	}
	
	
    public function indexAction() 
    {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		 
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );		
		$objError = new Zend_Session_Namespace ( PS_App_Error );	
		$objSess = new Zend_Session_Namespace(PS_Front_App_Auth);							
		
												
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';		    	
    }		
	
}
?>
