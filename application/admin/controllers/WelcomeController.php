<?php

class Admin_WelcomeController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
					 
	/* index Action */
	public function indexAction(){			
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = "Welcome";
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();				
		
		$objModel = new Models_Frontuser ();	
												
		$objLandlords = $objModel->countlandlords();
		$objTenant = $objModel->counttenant();
			
		$this->view->arrDataList = $objLandlords;
		$this->view->arrList = $objTenant;												
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = "";				
	}
					
	
	
}
?>