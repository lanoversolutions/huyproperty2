<?php

class Admin_PropertyController extends PS_Controller_Action 
{	
	function init() {
		parent::init ();
		$actionName = $this->getRequest ()->getActionName ();
		$controllerName = $this->getRequest ()->getControllerName ();
		$this->view->actionName = $actionName;
		$this->view->controllerName = $controllerName;		
	}
	
				 
	/* index Action */
	public function landlordindexAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORD');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_LANDLORD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Property ();		
		//$objForm = new Models_Form_Frontuser();
	   //$objForm->Listsearch();
		
		$arrStates = $objModel->getStateCombobox();
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getLanlordList ( $searchText, $searchType, $sortby );
		
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		$this->view->currentAction = 'landlordindex';
		unset ( $objModel, $objSelect, $objPaginator );	
	}
	
		/**
	 * The "Propertydetails" action is use to display a propertydetails
	 *
	 * This action to use the display property details.
	 
	 * via the following urls:
	 *
	 * /property/propertylisting/propertydetails
	 *
	 * @return void
	 */
	 /* propertylisting Action Start*/

	public function propertydetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_PROPERTY');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Property ();
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchdetails ( $id );
		$arrStates = $objModel->getStateCombobox();
					
		$this->view->arrDataList = $arrData;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';	
		$this->view->id = $id;
		$this->view->arrStates = $arrStates;
		$this->view->currentAction = 'propertylisting';
		unset ($objModel, $objRequest, $objTranslate );
					
	}

	
	

	/* index Action */
	public function tenantindexAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_TENANTREGISTRATION');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_TENANTREGISTRATION');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Tenantregister ();		
		//$objForm = new Models_Form_Frontuser();
		//$objForm->Listsearch();
		
		$objModelProperty = new Models_Property();
		$arrStates = $objModelProperty->getStateCombobox();
		//_pr($arrStates,1);
		
		$CurrentPageNo = $this->_getParam ( 'page' );
		$CurrentPageNo = ($CurrentPageNo == '') ? '1' : $CurrentPageNo;
		$this->view->current_page = $CurrentPageNo;			
		$sortby = trim ( $this->_getParam ( 'sortby' ) );
		$pagingExtraVar = array ();
		$searchText = '';
		$searchType = '';
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();
			if(isset($formData['search'])){
				if(isset($formData['txtsearch']))
					$searchText = $formData['txtsearch'];
	
				if(isset($formData['searchtype']))
					$searchType = $formData['searchtype'];															
				
				if (isset($formData['txtsearch']) && isset($formData['searchtype'])) {
					$pagingExtraVar = array ('txtsearch' => $searchText, 'searchuser_type' => $searchType, 'sortby' => $sortby );
				}
				
			}
		}
		
		if ($sortby != '')
			$arrSortBy = array ('sortby' => $sortby );
		else
			$arrSortBy = array ();
						
		$objSelect = $objModel->getTenantList ( $searchText, $searchType, $sortby );
		
		$objPaginator = Zend_Paginator::factory ( $objSelect );
		$objPaginator->setItemCountPerPage ( $this->getSiteVar ( 'PAGING_VARIABLE' ) );
		$objPaginator->setPageRange ( $this->getSiteVar ( 'TOTAL_PAGE_IN_GROUP' ) );
		$objPaginator->setCurrentPageNumber ( $this->_getParam ( 'page' ) );
		$this->view->pagingExtraVar = array_merge ( $this->getExtraVar (), $pagingExtraVar, $arrSortBy );
		$this->view->objPaginator = $objPaginator;
		$this->view->arrDataList = $objPaginator->getItemsByPage ( $objPaginator->getCurrentPageNumber () );
		$this->view->arrStates = $arrStates;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->sortby = $sortby;						
		//$this->view->objForm = $objForm;
		$this->view->currentAction = 'tenantindex';
		unset ( $objModel, $objSelect, $objPaginator );	
	}
		




		/**
	 * The "Listing" action is use to display a tenantdetails
	 *
	 * This action to use the list data
	 
	 * via the following urls:
	 *
	 * /tenant/tenantdetails
	 *
	 * @return void
	 */
	 /* tenantdetails Action Start*/
	public function tenantdetailsAction(){			
		
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_HEADING_TENANT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objSess = new Zend_Session_Namespace ( PS_Front_App_Auth );
		$objRequest = $this->getRequest ();
		
		$objModel = new Models_Tenantregister ();
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchTenant ( $id );
	    
		$objModelProperty = new Models_Property();
		$arrStates = $objModelProperty->getStateCombobox();

					
		$this->view->arrDataList = $arrData;
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->arrStates = $arrStates;	
		$this->view->id = $id;
		$this->view->currentAction = 'tenantdetails';
		unset ($objModel, $objRequest, $objTranslate );
					
	}
	/*Listing Action End*/
	
	
	
	/**
	 * The "add" action is use to add a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/add
	 *
	 * @return void
	 */
	/*Add pagesadd Action Start*/
	public function addAction() {		
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER_ADD');
		$this->view->cHeadingTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER_ADD');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Frontuser ();
		$objForm = new Models_Form_Frontuser ();					
		
		if ($objRequest->isPost ()) {
			$formData = $objRequest->getPost ();			
			
			if ($objForm->isValid ( $formData )) {
																		
					$objModel->saveData ( $formData );
					$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_USER_INSERT');
					$objError->messageType = 'confirm';
					$this->_redirect ( "/admin/user" );
					
			} else {
				$objForm->populate ( $formData );
				$objError->message = formatErrorMessage ( $objForm->getMessages () );
				$objError->messageType = 'error';
			}
		}
				
		$this->view->message = $objError->message;
		$this->view->messageType = $objError->messageType;
		$objError->message = "";
		$objError->messageType = '';
		$this->view->currentAction = 'Add';
		$this->view->objForm = $objForm;
	}
	/*Add pagesadd Action End*/
	
	
	
		/**
	 * The "edit" action is use to edit a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/edit
	 *
	 * @return void
	 */
	/*Edit Pagesedit Action Start*/

	
		public function changestatusAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		//$this->view->siteTitle = $objTranslate->translate('ADMIN_LABEL_PAGETITLE_USER_EDIT');
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Property ();		
		
		$id = $objRequest->id;										
		$arrData = array ();
		$arrData = $objModel->fetchstatus( $id );																
		//_pr($arrData,1);
		if(!empty($arrData)){

				if($arrData['property_status'] == 0 ){
					$formData['property_status']='1';
				}else{
					$formData['property_status']='0';
				}
				//_pr($arrData['property_status']);
			//_pr($formData,1);
			$objModel->updatestatus ( $formData,$id);
			
			$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_SAVED');
			$objError->messageType = 'confirm';	
				    $this->_redirect ( "/admin/property/landlordindex" );
		} else {
			$objError->message = formatErrorMessage ( $objForm->getMessages () );
			$objError->messageType = 'error';
			$this->_redirect ( "/admin/property/landlordindex" );											
		}
				
		//exit;		
	}

	
	/**
	 * The "delete" action is use to delete a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/delete
	 *
	 * @return void
	 */
	/*Delete Pagesdelete Action Start*/
	public function deletelandlordAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		
		$objModel = new Models_Property ();						
		
		$id = $objRequest->id;						
		$objModel->deleteproertyData($id);	

		
		$objModelPropertyAminities = new Models_PropertyAmenities ();
		$objModelPropertyAminities->deleteData($id);	
		
		$objModelPropertyPhoto = new Models_PropertyPhoto ();
		$objModelPropertyPhoto->deleteData($id);	
		
		
		$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_USER_DELETE');
		$objError->messageType = 'confirm';	
		
		$this->_redirect ( "/admin/property/landlordindex" );				
		exit;
	}
	/*Delete Pagesdelete Action End*/



	/**
	 * The "delete" action is use to delete a Page
	 *
	 * Assuming the default route and default router, this action is dispatched 
	 * via the following urls:
	 *
	 * /pages/delete
	 *
	 * @return void
	 */
	/*Delete Pagesdelete Action Start*/
	public function deleteltenantAction() {
		$objRequest = $this->getRequest ();
		$objTranslate = Zend_Registry::get ( PS_App_Zend_Translate );
		$objError = new Zend_Session_Namespace ( PS_App_Error );
		$objModel = new Models_Tenantregister ();						
		
		$id = $objRequest->id;			
		$objModel->deleteTenantData($id);		
		
		$objError->message = $objTranslate->translate('ADMIN_MSG_VALID_USER_DELETE');
		$objError->messageType = 'confirm';	
		
		$this->_redirect ( "/admin/property/tenantindex" );				
		exit;
	}
	/*Delete Pagesdelete Action End*/

	
}
?>