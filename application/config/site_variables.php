<?php
	///////////////////////
	// SITE CONFIGURATION//
	///////////////////////
	$path_http = pathinfo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	//define("SERVER_PATH", $path_http["dirname"]."/"); 								// server path is deined here
	$arrDirPath = explode("/", $path_http["dirname"]);
	if($arrDirPath[count($arrDirPath)-1] == "admin" || $arrDirPath[count($arrDirPath)-1] == "trunk"){
		define("SERVER_ROOT_PATH", substr(getcwd(), 0, (strlen(getcwd())-strlen($arrDirPath[count($arrDirPath)-1])))); // server root path is deined here
		$serverPath = $arrDirPath;
		array_pop($serverPath);
		$serverUrl = implode("/",$serverPath);
		define("SERVER_PATH", $serverUrl."/"); 		 					// server path is deined here
	}else{
		define("SERVER_ROOT_PATH", getcwd()."/"); 		  		// server root path is deined here
		$serverUrl = implode("/",$arrDirPath);
		define("SERVER_PATH", $serverUrl."/"); 								// server path is deined here
		$path_https = pathinfo('http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	}

	$path_https = pathinfo('https://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
	
	/*Email Send Configuration Detail*/
	define('CONFIGURATION_EMAIL_FROM','huyv76@gmail.com');
	define('CONFIGURATION_NAME_FROM','Huy Vo Admin');
	
	define('EMAIL_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/user/');
	define('EMAIL_CONTACT_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/contact/');
    define('EMAIL_TENANTCONTACT_BODY_PATH',APPLICATION_PATH.'/default/views/scripts/tenant/');

	/**/
	
	define("SERVER_SSL_PATH", $path_https["dirname"]."/");
	
	/* Site Namespace Variables
	 */
	define('PS_App_Zend_Translate','Zend_Translate');
	define('PS_App_Error','PS_Error');
	define('PS_App_Auth','PS_Auth');
	define('PS_App_Configuration','configuration');
	
	define('PS_App_DbAdapter','DbAdapter');	
	//define('PS_App_DbAdapter_Master','DbAdapterMaster');
	//define('PS_App_DbAdapter_Slave','DbAdapterSlave');
	
	define('PS_App_Acl','acl');
	define('PS_App_User_Info','PS_App_User_Info');
	define('PS_Front_App_Auth','PS_Front_App_Auth');
	
	/* Site Path Variables */					
	#---  JAVASCRIPT path ---#
	define('JS_PATH',SERVER_PATH.'js/');
	define('JS_ROOT_PATH',SERVER_ROOT_PATH.'js/');
	
	#----------email---------------#
	define('EMAIL_ROOT_PATH',SERVER_ROOT_PATH.'js/');
	
		#---  STYLE path ---#
	define('CKFINDER_ROOT_PATH',SERVER_ROOT_PATH.'ckfinder/');
	define('CKFINDER_PATH',SERVER_PATH.'ckfinder/');

	#---  STYLE path ---#
	define('STYLE_PATH',SERVER_PATH.'styles/');
	define('STYLE_ROOT_PATH',SERVER_ROOT_PATH.'styles/');	
	
	#---  IMAGE path ---#
	define('IMAGE_PATH',SERVER_PATH.'assets/images/');
	define('IMAGE_ROOT_PATH',SERVER_ROOT_PATH.'assets/images/');
	
	#---  CAPTCHA IMAGE path ---#
	define('CAPTCHA_PATH',SERVER_PATH.'captcha/');
	
		#---  IMAGE path ---#
	define('USER_IMAGES_PATH',SERVER_PATH.'img/');
	define('USER_IMAGES_ROOT_PATH',SERVER_ROOT_PATH.'img/');

	#--- Patient Direcetory Path ---#
	define('ACTIVATION_USER_CODE_PATH',SERVER_PATH.'default/user/activation/confirm_key/');
	
	#--- Patient Direcetory Path ---#
	define('CHECK_FORGOTPASS_PATH',SERVER_PATH.'default/user/checkpass/confirm_key/');
	
	#--- Invited person email activation Direcetory Path ---#
	define('ACTIVATION_INVITED_USER_CODE_PATH',SERVER_PATH.'default/register/eactivation/acode/');
	
	#---  USER IMAGE path ---#
	define('USER_IMAGE_PATH',IMAGE_PATH.'user/');
	define('USER_IMAGE_ROOT_PATH',IMAGE_ROOT_PATH.'user/');	
	
		#--- Patient Direcetory Path ---#
	define('GENARAL_MENU_PATH',SERVER_PATH.'application/views/scripts/property/');
				
							
	#---  Assets directory path ---#
	define('ASSETS_PATH',SERVER_PATH.'assets/');
	define('ASSETS_ROOT_PATH',SERVER_ROOT_PATH.'assets/');
	
	#--- Patient Direcetory Path ---#
	define('PATIENT_DOC_ROOT_PATH',ASSETS_ROOT_PATH.'patient_doc/');
			
	#---  Force download directory path ---#	
	define("FORCE_DOWNLOAD_PATH", ASSETS_PATH.'download.php');
	
		
	#--- GLOBAL ARRAYS ---#           
    // PAGING PARAM SETTINGS
    define('TOTAL_RECORDS_PER_PAGE',  '2');
    define('TOTAL_PAGE_IN_GROUP',  '3');
	
	
	#---  ARRAYS ---#
	//VoucherStatusTypeList array
		$VoucherStatusTypeList = array(
		""=>'Select',
		"I have a Voucher"=>'I have a Voucher',						
		"On Waiting List"=>'On Waiting List',
		"No Voucher"=>'No Voucher',
	   );
	
    //Property_Type array
	$arrProperty_Type  = array(''=>'Unknown',
				'1'=>'House',
				'2'=>'Apt',
				'3'=>'Condo/Coop',
				'4'=>'Duplex',
				'5'=>'Loft',
				'6'=>'Townhouse/Villa',
				'7'=>'Mobile Home',
				'8'=>'Row House',
				'9'=>'TriPlex',
				'10'=>'4 Plex',
				'11'=>'Low-Rise',
				'12'=>'High-Rise');
	
	//Lot_Size array
	$arrLot_Size = array(''=>'Unknown',
						'1'=>'None(apt,condo,etc)',
						'2'=>'< 1/4 Acre',
						'3'=>'1/4- 1/2 Acre',
						'4'=>'3/4 - 1 Acre',
						'5'=>'> 1 Acre');
							  
							  
	//LandlordType array						  
	$arrLandlordType = array(
		"0"=>'Yes',
		"1"=>'No',						
	   );
						  
	
	//LandlordType array				  
	$arrBedrooms = array(
				''=>'Unknown',
				'1'=>'Studio',
				'2'=>'1',
				'3'=>'2',
				'4'=>'3',
				'5'=>'4',
				'6'=>'5',
				'7'=>'6+');
	



	$arrBathrooms = array(
				''=>'Unknown',
				'1'=>'1',
				'2'=>'2',
				'3'=>'3',
				'4'=>'4',
				'5'=>'5',
				'6'=>'6+');


	//LandlordType array				  
	$arrLaundryType = array(
				'0'=>'Unknown',
				'1'=>'Washer / Dryer Hook-ups',
				'2'=>'Washer',
				'3'=>'Dryer',
				'4'=>'Onsite Laundry',
				'5'=>'Washer / Dryer');
				
				
	$arrHeatingType = array(
				'0'=>'Unknown',
				'1'=>'Baseboard',
				'2'=>'Boiler',
				'3'=>'Central',
				'4'=>'Furnace',
				'5'=>'Heat Pump',
				'6'=>'Radiator',
				'7'=>'Space',
				'8'=>'Window/Wall',
				'9'=>'None');
				

  $arrParking_Type = array(
				'0'=>'Unknown',
				'1'=>'1 - Carport',
				'2'=>'2 - Carport',
				'3'=>'1 - Car Garage',
				'4'=>'3 - Car Garage',
				'5'=>'Assigned',
				'6'=>'Unassigned',
				'7'=>'1 Space',
				'8'=>'2 Spaces',
				'9'=>'3+ Spaces',
				'9'=>'Street',
				'9'=>'Covered',
				'9'=>'Open',
				'9'=>'Driveway',
				'9'=>'None');
				
				
	$arrPatioType = array(
				'0'=>'Not Known',
				'1'=>'Porch',
				'2'=>'Balcony',
				'3'=>'Deck',
				'4'=>'Patio',
				'5'=>'None');
				
	$arrHeatingFuelType = array(
				''=>'Unknown',
				'1'=>'Natural Gas',
				'2'=>'Bottle Gas/Propane',
				'3'=>'Electric',
				'4'=>'Oil',
				'5'=>'None');
	
	$arrCityWaterType = array(
				''=>'Unknown',
				'1'=>'City Water',
				'2'=>'Well Water',
				'3'=>'None');

	
	$arrWaterType = array(
				''=>'Unknown',
				'1'=>'Natural Gas',
				'2'=>'Bottle Gas/Propane',
				'3'=>'Electric',
				'4'=>'None');
	
	$arrSewerType = array(
				''=>'Unknown',
				'1'=>'Public Sewer',
				'2'=>'Septic Tank');
				
				
	$arrCoolingType = array(
				''=>'Unknown',
				'1'=>'Central',
				'2'=>'Window/Wall',
				'3'=>'None');
				
	$arrUtilitiesType = array(
			'0'=>'Unknown',
			'1'=>'Yes',
			'2'=>'No');
			
	$arrUserOption = array(
			'1'=>'I m a Landlord, Apartment Community or Advertiser',
			'2'=>'I m a Tenant');
			
	$arrUserStatus = array(
			'1'=>'Active',
			'0'=>'InActive');
			
	$arrUsersOption = array(
			'0' => 'Not Selected',
			'1'=>'Landlord',
			'2'=>'Tenant');
	
    $Contactstatust = array(
			'1'=>'Read',
			'0'=>'Unread');

	$arrSquare_footage_min = array(
			'0'=>'Min',
			'250'=>'250',
			'500'=>'500',
			'750'=>'750',
			'1000'=>'1000',
			'1250'=>'1250',
			'1500'=>'1500',
			'1750'=>'1750',
			'2000'=>'2000',
			'2250'=>'2250',
			'2500'=>'2500+',
			);

	$arrSquare_footage_max = array(
			'0'=>'Max',
			'250'=>'250',
			'500'=>'500',
			'750'=>'750',
			'1000'=>'1000',
			'1250'=>'1250',
			'1500'=>'1500',
			'1750'=>'1750',
			'2000'=>'2000',
			'2250'=>'2250',
			'2500'=>'2500+',
			);

    //serching Property_Type array
	$arrSearchProperty_Type  = array(''=>'All',
				'1'=>'House',
				'2'=>'Apt',
				'3'=>'Condo/Coop',
				'4'=>'Duplex',
				'5'=>'Loft',
				'6'=>'Townhouse/Villa',
				'7'=>'Mobile Home',
				'8'=>'Row House',
				'9'=>'TriPlex',
				'10'=>'4 Plex',
				'11'=>'Low-Rise',
				'12'=>'High-Rise');
				

		//LandlordType array				  
	$arrSearchBedrooms = array(
				''=>'All',
				'1'=>'Studio',
				'2'=>'1+',
				'3'=>'2+',
				'4'=>'3+',
				'5'=>'4+',
				'6'=>'5+',
				'7'=>'6+');
			

	$arrSearchBathroom = array(
				''=>'All',
				'1'=>'1+',
				'2'=>'2+',
				'3'=>'3+',
				'4'=>'4+',
				'5'=>'5+',
				'6'=>'6+');


	//LandlordType array				  
	$arrBedroom = array(
				''=>'Unknown',
				'1'=>'Efficiency',
				'2'=>'1',
				'3'=>'2',
				'4'=>'3',
				'5'=>'4',
				'6'=>'5+');

				
	
	/*Define User type*/	
    define('USER_TYPE_FREE', 'FREE');
	define('USER_TYPE_PAIDTYPE1', 'PAIDTYPE1');
	
	/*Define Active & not active Variables*/
	define('USER_ACTIVE', '1');
	define('USER_NOT_ACTIVE', '0');
	
	/*User type*/
	define('LANDLORD', '1');
	define('TENANT', '2');
	