<?php

// APPLICATION CONSTANTS - Set the constants to use in this application.
// These constants are accessible throughout the application, even in ini
// files. We optionally set APPLICATION_PATH here in case our entry point
// isn't index.php (e.g., if required from our test suite or a script). 
defined('APPLICATION_PATH')
    or define('APPLICATION_PATH', dirname(__FILE__));

/*defined('APPLICATION_ENVIRONMENT')
    or define('APPLICATION_ENVIRONMENT', 'development'); // development*/
	
if(!defined('APPLICATION_ENVIRONMENT')){
	if($_SERVER['HTTP_HOST'] == 'localhost')
		define('APPLICATION_ENVIRONMENT', 'development'); // development
	else
		define('APPLICATION_ENVIRONMENT', 'production'); // production
}


// APPLICATION VARIABLES
include_once APPLICATION_PATH . "/config/site_variables.php";

// FRONT CONTROLLER - Get the front controller.
// The Zend_Front_Controller class implements the Singleton pattern, which is a
// design pattern used to ensure there is only one instance of
// Zend_Front_Controller created on each request.
$frontController = Zend_Controller_Front::getInstance();

// CONTROLLER DIRECTORY SETUP - Point the front controller to your action
// controller directory.
$controllerDir = array(
	'admin'     =>  APPLICATION_PATH . '/admin/controllers',
	'default'   =>  APPLICATION_PATH . '/default/controllers'
);

$frontController->setControllerDirectory($controllerDir);

// APPLICATION ENVIRONMENT - Set the current environment
// Set a variable in the front controller indicating the current environment --
// commonly one of development, staging, testing, production, but wholly
// dependent on your organization and site's needs.
$frontController->setParam('env', APPLICATION_ENVIRONMENT);

// LAYOUT SETUP - Setup the layout component
// The Zend_Layout component implements a composite (or two-step-view) pattern
// In this call we are telling the component where to find the layouts scripts.
$options = array(
    'layoutPath' => APPLICATION_PATH . '/layouts/default',
);
Zend_Layout::startMvc($options);

// VIEW SETUP - Initialize properties of the view object
// The Zend_View component is used for rendering views. Here, we grab a "global"
// view instance from the layout object, and specify the doctype we wish to
// use -- in this case, XHTML1 Strict.
$view = Zend_Layout::getMvcInstance()->getView();
$view->doctype('XHTML1_TRANSITIONAL');
$view->setEscape("escapeString");

// JQUERY SETUP - Initialize properties of the Jquery
// It can be used throughout the applicatin
$view->addHelperPath("ZendX/JQuery/View/Helper", "ZendX_JQuery_View_Helper");

$viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
$viewRenderer->setView($view);
Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

// CONFIGURATION - Setup the configuration object
// The Zend_Config_Ini component will parse the ini file, and resolve all of
// the values for the given section.  Here we will be using the section name
// that corresponds to the APP's Environment
$configuration = new Zend_Config_Ini(APPLICATION_PATH . '/config/application.ini', APPLICATION_ENVIRONMENT);

// DATABASE ADAPTER - Setup the database adapter
// Zend_Db implements a factory interface that allows developers to pass in an
// adapter name and some parameters that will create an appropriate database
// adapter object.  In this instance, we will be using the values found in the
// "database" section of the configuration obj.
//_pr($configuration->database->params->adapter,1);

$dbAdapter = Zend_Db::factory($configuration->database);
$dbAdapter->query('SET NAMES utf8'); //set utf8 charater set for DB

//$dbAdapterMaster = Zend_Db::factory($configuration->database->params->adapter,$configuration->database->params->db1);
//$dbAdapterSlave = Zend_Db::factory($configuration->database->params->adapter,$configuration->database->params->db2);


// DATABASE TABLE SETUP - Setup the Database Table Adapter
// Since our application will be utilizing the Zend_Db_Table component, we need
// to give it a default adapter that all table objects will be able to utilize
// when sending queries to the db.
Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapter);
//Zend_Db_Table_Abstract::setDefaultAdapter($dbAdapterMaster);


$objLocale = new Zend_Locale();
$objTranslate = new Zend_Translate('array', '../language/english.php', 'en');

if (!$objTranslate->isAvailable($objLocale->getLanguage()))
{
	$objTranslate->setLocale('en');
} else {
	$objTranslate->setLocale('en');
}

// REGISTRY - setup the application registry
// An application registry allows the application to store application
// necessary objects into a safe and consistent (non global) place for future
// retrieval.  This allows the application to ensure that regardless of what
// happends in the global scope, the registry will contain the objects it
// needs.
$registry = Zend_Registry::getInstance();
$registry->set(PS_App_Configuration, $configuration);
$registry->set(PS_App_DbAdapter, $dbAdapter);
//$registry->set(PS_App_DbAdapter_Master, $dbAdapterMaster);
//$registry->set(PS_App_DbAdapter_Slave, $dbAdapterSlave);
$registry->set(PS_App_Zend_Translate, $objTranslate);

//Caching
/*$frontendOptions = array(
						'lifetime'					=> 25200,
	    				'automatic_serialization'	=> true
);
$backendOptions  = array(
	   					 'cache_dir'=> APPLICATION_PATH . '/cache'
	   					 );
	   					 
$cache = Zend_Cache::factory(
						'Core',
	                    'File',
	   					 $frontendOptions,
	   					 $backendOptions
);
//Cache table metadata
Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);*/

/*
$options = array(
    // 'jquery_path' => 'http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js',
    'plugins' => array('Variables', 
                       'Html', 
                       'Database' => array('adapter' => array('standard' => $dbAdapterMaster)), 
                       'File' => array('base_path' => 'path/to/application/root'), 
                       'Memory', 
                       'Time', 
                       'Registry', 
                       'Cache' => array('backend' => 'true'), 
                       'Exception')
);

$debug = new ZFDebug_Controller_Plugin_Debug($options);

$frontController = Zend_Controller_Front::getInstance();
$frontController->registerPlugin($debug);
*/
$router = new Zend_Controller_Router_Rewrite();
$request =  new Zend_Controller_Request_Http();

$router->route($request);

//custom routing [STARTS] 
$frontrouter = $frontController->getRouter();

//for how to apply page 
$route_product_home = new Zend_Controller_Router_Route_Static(
	'how-to-apply.html',
	array('module' => 'default', 'controller' => 'agency', 'action' => 'index')
);
$frontrouter->addRoute('how_to_apply', $route_product_home);
//custom routing[ENDS]


//for how to about page 
$route_product_about = new Zend_Controller_Router_Route_Static(
	'About-Us.html',
	array('module' => 'default', 'controller' => 'page', 'action' => 'about')
);
$frontrouter->addRoute('about_us', $route_product_about);
//custom routing[ENDS]


//for how to apply page 
$route_product_terms = new Zend_Controller_Router_Route_Static(
	'Terms-and-Conditions.html',
	array('module' => 'default', 'controller' => 'page', 'action' => 'termsconditions')
);
$frontrouter->addRoute('terms_conditions', $route_product_terms);
//custom routing[ENDS]


//for how to apply page 
$route_product_privacypolicy = new Zend_Controller_Router_Route_Static(
	'Privacy-Policy.html',
	array('module' => 'default', 'controller' => 'page', 'action' => 'privacypolicy')
);
$frontrouter->addRoute('privacy_policy', $route_product_privacypolicy);
//custom routing[ENDS]


//for how to apply page 
$route_product_help = new Zend_Controller_Router_Route_Static(
	'Help.html',
	array('module' => 'default', 'controller' => 'page', 'action' => 'help')
);
$frontrouter->addRoute('help', $route_product_help);
//custom routing[ENDS]


//for how to contact page 
$route_product_contact = new Zend_Controller_Router_Route_Static(
	'Contact-Us.html',
	array('module' => 'default', 'controller' => 'contact', 'action' => 'index')
);
$frontrouter->addRoute('contact_us', $route_product_contact);
//custom routing[ENDS]

//for how to registeration page 
$route_product_register = new Zend_Controller_Router_Route_Static(
	'Registration.html',
	array('module' => 'default', 'controller' => 'user', 'action' => 'register')
);
$frontrouter->addRoute('registeration', $route_product_register);
//custom routing[ENDS]


//for how to login page 
$route_product_login = new Zend_Controller_Router_Route_Static(
	'Login.html',
	array('module' => 'default', 'controller' => 'user', 'action' => 'login')
);
$frontrouter->addRoute('login', $route_product_login);
//custom routing[ENDS]

//for state name
$route_state_name = new Zend_Controller_Router_Route_Regex(
        'how-to-apply/(.+)\.html',
       array('module' => 'default', 'controller' => 'agency', 'action' => 'state', 'filename' => ''),array(                
		1 => 'filename'
    ),
	'how-to-apply/%s.html'
);
$frontrouter->addRoute('state_name', $route_state_name);



//custom routing[ENDS]






$moduleName = $request->getModuleName();

// ZEND ACL 
$helper= new PS_Controller_Helper_Acl();
$helper->setRoles();
$helper->setResources();
$helper->setPrivilages();
$helper->setAcl();

$frontController->registerPlugin(new PS_Controller_Plugin_Acl());
//$frontController->registerPlugin(new PS_Controller_Plugin_Noroute());
//$frontController->setParam('noErrorHandler', true);

//$frontController->registerPlugin(new PS_Form_Decorator_PSWrapper());

// CLEANUP - remove items from global scope
// This will clear all our local bootsrap variables from the global scope of
// this script (and any scripts that called bootstrap).  This will enforce
// object retrieval through the Applications's Registry

//set default timezone  
date_default_timezone_set('America/Mexico_City');

//Facebook configration 
Zend_Registry::set('fbconfig', $configuration->fb);

unset($frontController, $view, $dbAdapter, $registry);
