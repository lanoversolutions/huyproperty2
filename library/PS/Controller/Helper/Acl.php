<?php
class PS_Controller_Helper_Acl
{
	protected $_acl;
	public function __construct()
	{
		$this->_acl = new Zend_Acl();
	}
	
	public function setRoles()
	{    	
	  	$this->_acl->addRole(new Zend_Acl_Role('god'));
		$this->_acl->addRole(new Zend_Acl_Role('admin'));
		$this->_acl->addRole(new Zend_Acl_Role('basic'));
		$this->_acl->addRole(new Zend_Acl_Role('salesman'));
		$this->_acl->addRole(new Zend_Acl_Role('guest'));
	}
	
	public function setResources()
	{		
		//$this->_acl->add(new Zend_Acl_Resource(array('admin-adminuser-index','ajax-adminuser-index','ajax-adminuser-add','ajax-adminuser-edit','ajax-adminuser-detail','ajax-adminuser-delete','ajax-adminuser-status','admin-distributor-index','ajax-distributor-index','ajax-distributor-add','ajax-distributor-edit','ajax-distributor-detail','ajax-distributor-delete','admin-ipad-index','ajax-ipad-index','ajax-ipad-add','ajax-ipad-edit','ajax-ipad-detail','ajax-ipad-delete')));		
		
						
	}
	
	public function setPrivilages()
	{
		$this->_acl->allow('god');	  		
		$this->_acl->allow('admin');
		$this->_acl->deny('admin',null,array('admin-vehicle-index'));				  	 	  				
		
		$this->_acl->allow('basic',null,array('admin-index-index','admin-index-logout','admin-adminuser-index','ajax-adminuser-index','ajax-adminuser-detail','admin-distributor-index','ajax-distributor-detail','admin-ipad-index','ajax-ipad-index','ajax-ipad-detail','admin-vehicle-index'));			    	
			
		$this->_acl->allow('salesman',null,array('admin-index-index','admin-index-logout','admin-adminuser-index','ajax-adminuser-detail','admin-distributor','ajax-distributor-detail','admin-distributor-index','admin-ipad-index','admin-vehicle-index'));								
		
		$this->_acl->allow('guest');	
		
	}
	
	public function setAcl()
	{
		Zend_Registry::set('acl',$this->_acl);
	}
}
?>