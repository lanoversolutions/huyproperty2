<?php 
class PS_Controller_Plugin_Acl extends Zend_Controller_Plugin_Abstract
{
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		
			$acl = Zend_Registry::get('acl');		
			$usersNs = new Zend_Session_Namespace("PS_Auth");
			
			if($usersNs->user_type==''){
				$roleName='guest';
			} else {
				$roleName=$usersNs->user_type;
			}			
			
			$privilageName  = $request->getModuleName();
			$privilageName .= "-".$request->getControllerName();
			$privilageName .= "-".$request->getActionName();			
			
			//echo $privilageName;exit; 
			
			if(!$acl->isAllowed($roleName,null,$privilageName)){								
				$request->setModuleName('ajax');
				$request->setControllerName('error');
				$request->setActionName('accesscontrol');
			}																			
		
	}
}
?>