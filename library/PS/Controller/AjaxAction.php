<?php

class PS_Controller_AjaxAction extends PS_Controller_Action {

    function init()
	{
		parent::init();
		$this->getHelper('layout')->disableLayout();
		//$this->getHelper('viewRenderer')->setNoRender();
		if($this->getRequest()->isXmlHttpRequest() === false ) {
    		$this->_redirect("/");
    	}
    }
}