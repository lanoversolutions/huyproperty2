// JavaScript Document
jQuery(document).ready(function() {											
	
	// binds form submission and fields to the validation engine
	jQuery("#PSForm").validationEngine();		
	
	$.callSelectPatient = function(id) {
		$("#refered_id").val(id);
		$('#PSForm_search').submit();
	};
	
	$.callDeleteData = function(id) {
		
		$('<input type="text" />').attr({ name: 'delete_id', id: 'delete_id'}).appendTo("#PSForm_search");
		$("#delete_id").val(id);
		$('#PSForm_search').submit();
	};
	
		
});